var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compression = require('compression');
var logger = require('morgan');
var http = require('http');
var proxiedHttp = require('findhit-proxywrap').proxy(http, {
    strict: false
});
var app = express();
var srv = proxiedHttp.createServer(app); // instead of http.createServer(app)
var io = require('socket.io')(srv);
var flash = require('connect-flash');
var mongoose = require('mongoose');
var passport = require('passport');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);

var config = (require(path.join(__dirname, '/config/config')));


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));

app.use(function(req, res, next) {
    if (req.connection.proxyPort === 80) {
        res.redirect('https://' + req.get('Host') + req.url);
    }
    else
        next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());

app.use(compression({
    threshold: 0
}));
app.use(express.static(__dirname + '/public'));

var sessionMiddleware = session({
    secret: 'Az4YdlhJHCvTRWbrzHXy',
    store: new MongoStore({
        url: config.dburl,
        ttl: 3600 //1 hour exipry 3600
    })
});

app.use(sessionMiddleware);

app.use(passport.initialize());
app.use(passport.session());


// Using the flash middleware provided by connect-flash to store messages in session and displaying in templates
app.use(flash());

// Initalize Mongojs
var collections = ['projects', 'events', 'news', 'users', 'siteStats', 'emailDomain'];
var db = require('mongojs').connect(config.dburl, collections);

// Connect to DB
mongoose.connect(config.dburl);

db.createCollection("projects");
db.createCollection("events");
db.createCollection("news");
db.createCollection("siteStats");
db.createCollection("emailDomain");
db.createCollection("users");

db.users.find({
    username: "Admin"
}, function(err, user) {
    if (err) console.log('Error finding admin user: ' + err);
    else if (!user.length) {
        console.log('no admin user in the database');
        //pass = systemadmin@1!
        var adminUser = {
            _id: (1e4 * (Date.now() + Math.random())).toString(16),
            firstName: "System",
            lastName: "Admin",
            username: "Admin",
            password: "$2a$10$QVoYAsEO2WniHbWCBnOD.O5bfse4RFYN292m5l6Lvrnphpjqo9yxK",
            Access: 5,
            OpCo: "BA"
        };
        db.users.insert(adminUser);
    }
    else console.log('found admin user');
});

var routes = require(path.join(__dirname, 'routes/index'))(passport, db);
app.use('/', routes);

// Initialize Passport
var initPassport = require(path.join(__dirname, 'passport/init'));
initPassport(passport, db);

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// development error handler. Will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}


module.exports = app;

//socket shit
var socketFns = require(path.join(__dirname, 'socketFunctions.js'));
socketFns(db, io);

/*http.listen(config.port, function() {
    console.log('listening on *:' + config.port);
});*/

srv.listen(config.port, function() {
    console.log('listening on *:' + config.port);
});

