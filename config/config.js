var config = {
};

switch (process.env.NODE_ENV) {
	
	case 'development':
		config = {
			port: 8080,
			dburl: 'mongodb://mkelso633:pmEPNcsqNbfFHrvsNK50@ds053429.mongolab.com:53429/inodata',
			passport: {
				baSAML: {
					path: 'https://innovations-security-mkelso633.c9.io/balogin/callback',
					entryPoint: 'https://fed-uat.baplc.com/fed/idp/samlv20',
					issuer: 'https://innovations-security-mkelso633.c9.io',
					baMetadataURL: 'https://fed-uat.baplc.com/fed/idp/metadata',
					logoutUrl: 'https://auth-uat.baplc.com/baAuthnApp/logout.jsp'
				},
				ibSAML: {
					path: 'https://innovations-security-mkelso633.c9.io/iblogin/callback',
					entryPoint: 'https://fed-uat.baplc.com/fed/idp/samlv20',
					issuer: 'https://innovations-security-mkelso633.c9.io',
					ibMetadataURL: 'https://fed-uat.baplc.com/fed/idp/metadata',
					logoutUrl: 'https://auth-uat.baplc.com/baAuthnApp/logout.jsp'
				}
			}
		}
		break;
		
	case 'production':
		config = {
			port: 8080,
			dburl: 'mongodb://mkelso633:pmEPNcsqNbfFHrvsNK50@ds053429.mongolab.com:53429/inodata',
			passport: {
				baSAML: {
					path: 'https://dashboard.iagdigitallabs.com/balogin/callback',
					entryPoint: 'https://fed.baplc.com/fed/idp/samlv20',
					issuer: 'https://dashboard.iagdigitallabs.com',
					baMetadataURL: 'https://fed.baplc.com/fed/idp/metadata',
					logoutUrl: 'https://auth.baplc.com/baAuthnApp/logout.jsp'
				},
				ibSAML: {
					path: 'https://dashboard.iagdigitallabs.com/iblogin/callback',
					entryPoint: 'https://fed.baplc.com/fed/idp/samlv20',
					issuer: 'https://dashboard.iagdigitallabs.com',
					ibMetadataURL: 'https://fed.baplc.com/fed/idp/metadata',
					logoutUrl: 'https://auth.baplc.com/baAuthnApp/logout.jsp'
				}
			}
		}
		break;
	default:
            throw new Error('YOU MUST SPECIFY AN ENVIRONMENT CONDITION. IE. NODE_ENV=development node app.js')
}


module.exports = config;