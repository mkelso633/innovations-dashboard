
var mongoose = require('mongoose');

module.exports = mongoose.model('User',{
	_id: String,
	username: String,
	password: String,
	firstName: String,
	lastName: String,
	Access: Number,
	OpCo: String,
	NewUser: Boolean,
	loginMethod: String
});