var config = require('../config/config');
var saml = require('passport-saml');
var fs = require("fs");
var xml2js = require('xml2js');
var parser = new xml2js.Parser();
var https = require("https");
var path = require('path');

module.exports = function(passport, db) {
    var cert = fs.readFileSync(path.join(__dirname, '../config/myCert.pem'), 'utf-8');
    var pvk = fs.readFileSync(path.join(__dirname, '../config/myKey.pem'), 'utf-8');

    var req = https.get(config.passport.baSAML.baMetadataURL, function(res) {
        // save the data
        var xml = '';
        res.on('data', function(chunk) {
            xml += chunk;
        });

        res.on('end', function() {
            // parse xml
            parser.parseString(xml, function(err, result) {
                if (err) {
                    console.log('Error Parsing BA Metadata ', err);
                }
                else {
                    var baplcCert = result['md:EntityDescriptor']['md:IDPSSODescriptor'][0]['md:KeyDescriptor'][0]['dsig:KeyInfo'][0]['dsig:X509Data'][0]['dsig:X509Certificate'][0];

                    passport.use('baSAML', new saml.Strategy({
                            entryPoint: config.passport.baSAML.entryPoint,
                            issuer: config.passport.baSAML.issuer,
                            cert: baplcCert,
                            callbackUrl: config.passport.baSAML.path,
                            decryptionPvk: pvk,
                            privateCert: pvk,
                            identifierFormat: null
                        },
                        function(user, done) {

                            db.users.findOne({
                                username: user.nameID
                            }, function(err, foundUser) {
                                if (err) console.log('error finding user (baLogin) ', err);
                                else {
                                    if (foundUser === null) {
                                        var newUser = {
                                            _id: (1e4 * (Date.now() + Math.random())).toString(16),
                                            username: user.nameID,
                                            Access: 2,
                                            NewUser: true,
                                            loginMethod: 'bSafe'
                                        };
                                        db.users.insert(newUser);

                                        return done(null, newUser);
                                    }
                                    else {

                                        return done(null, foundUser);
                                    }


                                }
                            });
                        }));
                }
            });
        });
    });
};