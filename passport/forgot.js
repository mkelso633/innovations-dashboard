var bCrypt = require('bcrypt-nodejs');
//nodemailer (e-mail) Setup
var nodemailer = require('nodemailer');
/*var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'matthewkelsoba@gmail.com',
        pass: 'BApassword12'
    }
});*/
var sesTransport = require('nodemailer-ses-transport');
var aws = require('aws-sdk');

var ses = new aws.SES({
    apiVersion: '2010-12-01',
    region: 'eu-west-1'
});

var transporter = nodemailer.createTransport(sesTransport({
    ses: ses
}));

var https = require("https");
var secret = '6LdIYAsTAAAAAM4ONlmvrzeMrPJh81xdrZ0ZZ-z6';

function verifyRecaptcha(key, ipAddress, callback) {
    https.get("https://www.google.com/recaptcha/api/siteverify?secret=" + secret + "&response=" + key + "&remoteip=" + ipAddress, function(res) {
        var data = "";
        res.on('data', function(chunk) {
            data += chunk.toString();
        });
        res.on('end', function() {
            try {
                var parsedData = JSON.parse(data);
                callback(parsedData.success);
            }
            catch (e) {
                callback(false);
            }
        });
    });
}


module.exports = function(db, req, res) {
    verifyRecaptcha(req.body["g-recaptcha-response"], req.ip, function(success) {
        if (success) {
            var username = req.body.username;
            var password = Math.random().toString(36).substring(7);

            db.users.findOne({
                    username: username
                }, function(err, data) {
                    if (err || data === null) {

                        console.log('[' + new Date() + '] user not found (forgot my pass) or Error : ' + err);
                        res.render('forgot', {
                            message: 'Error, user not found'
                        });
                    }
                    else {
                        db.users.update({
                            username: username
                        }, {
                            $set: {
                                password: createHash(password)
                            }
                        }, function(err) {

                            if (err) {
                                console.log('[' + new Date() + '] Error reseting password Error:' + err)
                            }
                            else {

                                transporter.sendMail({
                                    from: 'Digital Business Transformation digital.dashboard@connect.iairgroup.com',
                                    to: username,
                                    subject: 'Digital Business Innovations Dashboard password',
                                    html: '<img src="https://dashboard.iagdigitallabs.com/images/iag-logo-email.jpg" alt="IAG International Airlines Group" width="210" height="50" /><br/><p>Hello</p><p>Your password has been reset and is now set to: ' + password + '</p><p>To keep your password secure, please remember to change it to something unique the next time you log in by visiting the "My Profile" page.</p><p>Regards<br /><br />IAG Digital Business Innovations Team</p>',
                                }, function(error, info) {
                                    if (error) {
                                        console.log(error);
                                    }
                                    else {
                                        console.log('Message sent: ' + info);
                                    }
                                });


                            }

                            res.render('forgot', {
                                message: 'Please check your e-mail for your new password. Make sure to check the junk folder'
                            });

                        }); // end of update
                    }
                }) // end of findOne
        }
        else {
            res.render('forgot', {
                message: 'reCAPTCHA required'
            });
        }
    })

};
// Generates hash using bCrypt
var createHash = function(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}