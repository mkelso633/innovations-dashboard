var localLogin = require('./localLogin');
var localSignup = require('./localSignup');
var baLogin = require('./baLogin');
//var ibLogin = require('./ibLogin');

var User = require('../models/user');

module.exports = function(passport, db){

	// Passport needs to be able to serialize and deserialize users to support persistent login sessions
    passport.serializeUser(function(user, done) {
        done(null, user);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    // Setting up Passport Strategies for Login and SignUp/Registration
    localLogin(passport);
    localSignup(passport);
    baLogin(passport, db);
    //ibLogin(passport, db);


}