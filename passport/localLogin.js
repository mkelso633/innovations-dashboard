var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');
var bCrypt = require('bcrypt-nodejs');
var https = require("https");
var secret = '6LdIYAsTAAAAAM4ONlmvrzeMrPJh81xdrZ0ZZ-z6';

function verifyRecaptcha(key, ipAddress, callback) {
    https.get("https://www.google.com/recaptcha/api/siteverify?secret=" + secret + "&response=" + key + "&remoteip=" + ipAddress, function(res) {
        var data = "";
        res.on('data', function(chunk) {
            data += chunk.toString();
        });
        res.on('end', function() {
            try {
                var parsedData = JSON.parse(data);
                callback(parsedData.success);
            }
            catch (e) {
                callback(false);
            }
        });
    });
}

module.exports = function(passport) {
    passport.use('localLogin', new LocalStrategy({
            passReqToCallback: true
        },
        function(req, username, password, done) {
            verifyRecaptcha(req.body["g-recaptcha-response"], req.ip, function(success) {
                if (success) {

                    // check in mongo if a user with username exists or not
                    User.findOne({
                            'username': username,
                            'loginMethod': {
                                $ne: 'bSafe'
                            }
                        },
                        function(err, user) {
                            // In case of any error, return using the done method
                            if (err)
                                return done(err);
                            // Username does not exist, log the error and redirect back
                            if (!user) {
                                console.log('User Not Found with username ' + username);
                                return done(null, false, req.flash('message', 'Invalid username or password'));
                            }
                            // User exists but wrong password, log the error 
                            if (!isValidPassword(user, password)) {
                                console.log('Invalid Password');
                                return done(null, false, req.flash('message', 'Invalid username or password')); // redirect back to login page
                            }
                            // User and password both match, return user from done method
                            // which will be treated like success
                            return done(null, user);

                        }
                    )
                }
                else {
                    //recaptcha fail
                    return done(null, false, req.flash('message', 'reCAPTCHA required'));
                }
            })


        }));


    var isValidPassword = function(user, password) {
        return bCrypt.compareSync(password, user.password);
    }

}