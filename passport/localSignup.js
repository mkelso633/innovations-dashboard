var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');
var bCrypt = require('bcrypt-nodejs');
var _ = require('underscore');
var config = (require('../config/config'))
    //nodemailer (e-mail) Setup
var nodemailer = require('nodemailer');
/*var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'matthewkelsoba@gmail.com',
        pass: 'BApassword12'
    }
});*/
/* AWS email send */
var sesTransport = require('nodemailer-ses-transport');
var aws = require('aws-sdk');

var ses = new aws.SES({
    apiVersion: '2010-12-01',
    region: 'eu-west-1'
});

var transporter = nodemailer.createTransport(sesTransport({
    ses: ses
}));

// Nodejs encryption with CTR
var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'VM9XS9DKTJRuwwebqXIq';

function encrypt(text) {
    var cipher = crypto.createCipher(algorithm, password)
    var crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted;
}

var dburl = config.dburl;
var collection = ['emailDomain'];
var db = require('mongojs').connect(dburl, collection);

var https = require("https");
var secret = '6LdIYAsTAAAAAM4ONlmvrzeMrPJh81xdrZ0ZZ-z6';

function verifyRecaptcha(key, ipAddress, callback) {
    https.get("https://www.google.com/recaptcha/api/siteverify?secret=" + secret + "&response=" + key + "&remoteip=" + ipAddress, function(res) {
        var data = "";
        res.on('data', function(chunk) {
            data += chunk.toString();
        });
        res.on('end', function() {
            try {
                var parsedData = JSON.parse(data);
                callback(parsedData.success);
            }
            catch (e) {
                callback(false);
            }
        });
    });
}

module.exports = function(passport) {

    passport.use('localSignup', new LocalStrategy({
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) {
            verifyRecaptcha(req.body["g-recaptcha-response"], req.ip, function(success) {
                if (success) {
                    findOrCreateUser = function() {
                        // find a user in Mongo with provided username
                        User.findOne({
                            'username': username
                        }, function(err, user) {
                            // In case of any error, return using the done method
                            if (err) {
                                console.log('Error in SignUp: ' + err);
                                return done(err);
                            }
                            // already exists
                            if (user) {
                                console.log('User already exists with username: ' + username);
                                return done(null, false, req.flash('message', 'User Already Exists'));
                            }
                            else {
                                var opco = ''
                                var userEmailDomain = username.substr(username.indexOf("@"));
                                var accessGranted = false;

                                db.emailDomain.find({}, function(err, validOpcos) {
                                    if (err) {
                                        console.log('error finding validOpcos Email Domains');
                                        return done(null, false, req.flash('message', 'Error, please try again later'));
                                    }
                                    else {
                                        validOpcos.forEach(function(validOpco) {
                                            if (validOpco.domain === userEmailDomain) {
                                                accessGranted = true;
                                                opco = validOpco.name;
                                            }
                                        });

                                        //checks to make sure they of a valid opco
                                        if (!accessGranted) {
                                            return done(null, false, req.flash('message', 'E-mail must be of a valid opco'));
                                        }
                                        else {
                                            // if there is no user with that email plus they are from a valid opco
                                            // create the user
                                            var newUser = new User();

                                            // set the user's local credentials
                                            newUser._id = (1e4 * (Date.now() + Math.random())).toString(16);
                                            newUser.username = username;
                                            newUser.password = createHash(password);
                                            newUser.firstName = req.body.firstName;
                                            newUser.lastName = req.body.lastName;
                                            newUser.Access = 0;
                                            newUser.OpCo = opco;
                                            newUser.loginMethod = 'local';

                                            // save the user
                                            newUser.save(function(err) {
                                                if (err) {
                                                    console.log('Error in Saving user: ' + err);
                                                    throw err;
                                                }
                                                console.log('User Registration succesful');

                                                transporter.sendMail({
                                                    from: 'Digital Business Transformation digital.dashboard@connect.iairgroup.com',
                                                    to: username,
                                                    subject: 'Verify your Digital Business Innovations account',
                                                    html: '<img src="https://dashboard.iagdigitallabs.com/images/iag-logo-email.jpg" alt="IAG International Airlines Group" width="210" height="50" /><br/><p>Hello ' + newUser.firstName + '!</p><p>Thank you for signing up to the IAG Digital Business Transformation Dashboard. Please verify your e-mail address by <a href="https://dashboard.iagdigitallabs.com/verify/' + encrypt(newUser._id) + '">clicking here</a>. If the link doesn\'t work, please copy the following URL into your browser: https://dashboard.iagdigitallabs.com/verify/' + encrypt(newUser._id) + '</p><p>If you did not register for an account on the dashboard then please ignore this email and do not click on the link.<p>Regards<br /><br />IAG Digital Business Transformation Team</p>',
                                                }, function(error, info) {
                                                    if (error) {
                                                        console.log(error);
                                                    }
                                                    else {
                                                        console.log('Message sent: ' + info);
                                                    }
                                                });

                                                return done(null, false, req.flash('message', 'Before you can logon, please verify your details in the email just sent to you. Make sure to check the spam/junk folder.'));
                                            });
                                        }

                                    }
                                });
                            }
                        });
                    };

                }
                else {
                    //recaptcha fail
                    return done(null, false, req.flash('message', 'reCAPTCHA required'));
                }
                // Delay the execution of findOrCreateUser and execute the method
                // in the next tick of the event loop
                process.nextTick(findOrCreateUser);
            })
        }));

    // Generates hash using bCrypt
    var createHash = function(password) {
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    }



}
