(function() {
    var app = angular.module('AdminDashboard', ['adminController'])
})();

var adminController = angular.module('adminController', []);

adminController.controller('AdminController', function($scope, $location) {

    //temp for dev
    $scope.access = 5;
    $scope.hideFilterButton = true;
    $scope.mobile = false;

    $scope.addUser = {};
    $scope.emailDomainList = {};

    function checkAccess() {
        if ($scope.access !== undefined) {
            accessLoaded()
            clearInterval(checkInterval)
        }
    }

    var checkInterval = setInterval(function() {
        checkAccess();
    }, 200)

    function accessLoaded() {
        if ($scope.access !== 5) {
            $location.path('home');
        }
        else {
            socket.emit('findAllUserPermissionsRequest');
            socket.emit('emailDomainListRequest');
            socket.emit('backupDataRequest', 'projectsBackup');
            socket.emit('backupDataRequest', 'eventsBackup');
            socket.emit('backupDataRequest', 'newsBackup');
        }

    }

    $scope.confirmDeleteUser = function(whatToDelete, idToDelete) {
        $("#modalConfirmDeleteUser").foundation('reveal', 'open');
        $scope.whatToDelete = whatToDelete;
        $scope.itemToDelete = idToDelete;
    }
    $scope.closeDeleteModal = function() {
        $("#modalConfirmDeleteUser").foundation('reveal', 'close');
    }

    socket.on('findAllUserPermissionsResult', function(data) {
        $scope.userPermissions = data;
        $scope.$apply($scope.userPermissions)
    });


    $scope.changeAccessLevel = function(user, newLevel) {
        socket.emit('changeAccessLevel', user, newLevel);
    };

    $scope.deleteUser = function(itemToDelete) {
        var index = $scope.userPermissions.indexOf(itemToDelete);
        $scope.userPermissions.splice(index, 1);
        socket.emit('deleteUser', itemToDelete);
        $('#modalConfirmDeleteUser').foundation('reveal', 'close');
    }

    // add new User
    $scope.addNewUserModalOpen = function() {
        $scope.addUser.Access = 2;
    };
    $scope.addNewUser = function(user) {
        var error = false;

        if (user.firstName === undefined || user.firstName === '') {
            $scope.firstNameInvalid = true
            error = true;
        }
        else {
            $scope.firstNameInvalid = false
        }
        if (user.lastName === undefined || user.lastName === '') {
            $scope.lastNameInvalid = true
            error = true;
        }
        else {
            $scope.lastNameInvalid = false
        }
        if (user.username === undefined || user.username === '') {
            $scope.usernameInvalid = true
            error = true;
        }
        else {
            $scope.usernameInvalid = false
        }
        if (user.password === undefined || user.password === '') {
            $scope.passwordInvalid = true
            error = true;
        }
        else {
            $scope.passwordInvalid = false
        }
        if (user.OpCo === undefined || user.OpCo === '') {
            $scope.opcoInvalid = true
            error = true;
        }
        else {
            $scope.opcoInvalid = false
        }
        user.Access = parseInt(user.Access);

        if (error) {
            $scope.addUserAlert = 'Please fill out all the fields';
            $scope.showAlert();
            setTimeout(function() {
                $scope.hideAlert();
            }, 4000);
            $scope.$apply($scope.addUserAlert)
        }
        else {
            socket.emit('addNewUser', user);
        }

    }

    socket.on('addNewUserResponse', function(success, message) {
        $scope.addUserAlert = '';
        if (success) {
            // success message
            $scope.hideAlert();
            $("#alert-addUser").removeClass("alert").addClass("success");
            $scope.addUserAlert = message;
            setTimeout(function() {
                $scope.showAlert();
                $scope.addUser = {};
                $scope.addUser.Access = 2;
                $scope.$apply($scope.addUser)
            }, 200);
            setTimeout(function() {
                $("#alert-addUser").removeClass("success").addClass("alert");
                $scope.hideAlert();
            }, 4000);
        }
        else {
            $scope.addUserAlert = message;
            $scope.showAlert();
            setTimeout(function() {
                $scope.hideAlert();
            }, 4000);
        }
        $scope.$apply($scope.addUserAlert)
    });

    $scope.showAlert = function() {
        $("#alert-addUser").addClass("active");
    }
    $scope.hideAlert = function() {
        $("#alert-addUser").removeClass("active");
    }

    // email domain

    socket.on('emailDomainListResult', function(data) {
        $scope.emailDomainList = data;
        $scope.$apply($scope.emailDomainList)
    });
    $scope.addEmailDomain = function() {
        var newDomain = {
            _id: (1e4 * (Date.now() + Math.random())).toString(16),
            name: $scope.newEmailDomin.name,
            domain: $scope.newEmailDomin.domain
        };
        $scope.newEmailDomin = {};
        $scope.emailDomainList.push(newDomain);
        socket.emit('addEmailDomain', newDomain);
    };

    $scope.confirmDeleteDomain = function(id) {
        $('#modalConfirmDeleteDomain').foundation('reveal', 'open');
        $scope.domainToDelete = id;
    };
    $scope.deleteDomain = function() {
        $scope.emailDomainList = removeFunction($scope.emailDomainList, '_id', $scope.domainToDelete);
        socket.emit('deleteEmailDomain', $scope.domainToDelete);
        $('#modalConfirmDeleteDomain').foundation('reveal', 'close');
    };
    $scope.closeDeleteDomainModal = function() {
        $("#modalConfirmDeleteDomain").foundation('reveal', 'close');
    }

    //data backup and restore
    var date = new Date().toDateString()

    socket.on('backupDataResult', function(data, table) {

        data = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(data));

        if (table === 'projectsBackup')
            $("#projectsBackup").attr({
                href: "data:" + data,
                download: date + ' Projects Backup Data.json'
            });
        if (table === 'eventsBackup')
            $("#eventsBackup").attr({
                href: "data:" + data,
                download: date + ' Events Backup Data.json'
            });
        if (table === 'newsBackup')
            $("#newsBackup").attr({
                href: "data:" + data,
                download: date + ' News Backup Data.json'
            });

    });

    socket.on('finishedRestore', function(table) {
        alert('finished Restore for ' + table);
    });

    setTimeout(function() {
        $(document).foundation('reveal', 'reflow');
    }, 50);

    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };
    if (isMobile.any()) {
        $scope.mobile = true;
    }
    
    
    socket.emit('siteStatsInsert', 'Admin', $scope.mobile);

});
var tableToInsert = '';

function handleFiles(files, table) {

    // Check for the various File API support.
    if (window.FileReader) {
        // FileReader are supported.
        tableToInsert = table;
        getAsText(files[0]);

    }
    else {
        alert('FileReader are not supported in this browser.');
    }
}

function getAsText(fileToRead) {

    var reader = new FileReader();

    // Read file into memory as UTF-8
    reader.readAsText(fileToRead);
    // Handle errors load
    reader.onload = loadHandler;
    reader.onerror = errorHandler;
    /*uploadMessage.innerHTML = "Getting file as text";
    uploadProgress.style.width = "20%";*/
}

function loadHandler(event) {
    var data = event.target.result;
    processData(data);
}

function processData(data) {

    data = JSON.parse(data);

    socket.emit('restoreData', tableToInsert, data)


}


function errorHandler(evt) {
    if (evt.target.error.name == "NotReadableError") {
        alert("Can not read file !");
    }
}
