// Defines all the required modules and handles routing 
(function() {
    var app = angular.module('Dashboard', ['ngRoute', 'globalController', 'homeController', 'analyticsController', 'Dashboard.filters', 'directivesModule', 'ngCsv', 'projectsController', 'stakeholderController', 'editProjectsController', 'eventsController', 'ui.unique', 'newsController', 'angularUtils.directives.dirPagination', 'ckeditor', 'ui.calendar', 'profileController', 'isteven-multi-select'])

    // App route provider
    app.config(['$routeProvider', '$compileProvider',
        function($routeProvider, $compileProvider) {
            $compileProvider.debugInfoEnabled(false);
            $routeProvider.
            when('/home', {
                templateUrl: 'views/home.html',
                controller: 'HomeController'
            }).
            when('/Scout', {
                templateUrl: 'views/projects.html',
                controller: 'ProjectsController'
            }).
            when('/Alpha', {
                templateUrl: 'views/projects.html',
                controller: 'ProjectsController'
            }).
            when('/Beta', {
                templateUrl: 'views/projects.html',
                controller: 'ProjectsController'
            }).
            when('/Live', {
                templateUrl: 'views/projects.html',
                controller: 'ProjectsController'
            }).
            when('/Events', {
                templateUrl: 'views/events.html',
                controller: 'EventsController'
            }).
            when('/projects/:param', {
                templateUrl: 'views/projects.html',
                controller: 'ProjectsController'
            }).
            when('/edit/:param', {
                templateUrl: 'views/editProjects.html',
                controller: 'EditProjectsController'
            }).
            when('/add', {
                templateUrl: 'views/editProjects.html',
                controller: 'EditProjectsController'
            }).
            when('/stages', {
                templateUrl: 'views/stages.html',
            }).
            when('/goldenRules', {
                templateUrl: 'views/goldenRules.html',
            }).
            when('/humanApi', {
                templateUrl: 'views/humanApi.html',
            }).
            when('/listeningPost', {
                templateUrl: 'views/listeningPost.html',
            }).
            when('/innovationTool', {
                templateUrl: 'views/innovationTool.html',
            }).
            when('/Seismograph', {
                templateUrl: 'views/news.html',
                controller: 'NewsController'
            }).
            when('/Wire', {
                templateUrl: 'views/news.html',
                controller: 'NewsController'
            }).
            when('/Catapult', {
                templateUrl: 'views/news.html',
                controller: 'NewsController'
            }).
            when('/article/:param', {
                templateUrl: 'views/article.html',
                controller: 'NewsController'
            }).
            when('/stakeholder', {
                templateUrl: 'views/stakeholder.html',
                controller: 'stakeholderController'
            }).
            when('/analytics', {
                templateUrl: 'views/analytics.html',
                controller: 'AnalyticsController'
            }).
            when('/profile', {
                templateUrl: 'views/profile.html',
                controller: 'ProfileController'
            }).
            when('/SeismographLearnMore', {
                templateUrl: 'views/seismographLearnMore.html',
            }).
            when('/CatapultLearnMore', {
                templateUrl: 'views/catapultLearnMore.html',
            }).
            when('/', {
                templateUrl: 'views/home.html',
                controller: 'HomeController'
            }).
            when('/404', {
                templateUrl: 'views/404.html',
            }).
            otherwise({
                redirectTo: '/404'
            });
        }
    ]);

})();
