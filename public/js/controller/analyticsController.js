var analyticsController = angular.module('analyticsController', []);

analyticsController.controller('AnalyticsController', function($scope, $location, $filter) {


    $scope.filterStatusArray = ['Active', 'Progressed', 'Killed', 'Predicted'];

    var checkWhoIsOnline;

    function checkAccess() {
        if ($scope.access !== undefined) {
            accessLoaded()
            clearInterval(checkInterval)
        }
    }

    var checkInterval = setInterval(function() {
        checkAccess();
    }, 200)

    function accessLoaded() {
        if ($scope.access < 3) {
            $location.path('home');
        }
        else {
            socket.emit('siteStatsPageViewsRequest', dateFrom, dateTo);
            socket.emit('siteStatsPopularUserRequest')
            socket.emit('siteStatsTrafficSourcesRequest');
            socket.emit('siteStatsActivtyPerOpcoRequest');
            socket.emit('howManyOnlineCountRequest');
            socket.emit('findAnalyticsOfProjects', $scope.filterStatusArray);
            checkWhoIsOnline = setInterval(function() {
                socket.emit('howManyOnlineCountRequest');
            }, 1000);
        }

    }

    $scope.$on('$locationChangeSuccess', function(event) {
        clearInterval(checkWhoIsOnline);
    })

    setTimeout(function() {


        /*  $(".datepicker").datepicker({
            dateFormat: "dd/mm/yy",
 
        }); */

    }, 30);

    $scope.modalShown = false;
    $scope.toggleModal = function() {
        $scope.modalShown = !$scope.modalShown;
    };

    // Status filter (Active, progressed etc)
    $scope.selectStatusFilter = function(status) {
        if (_.contains($scope.filterStatusArray, status)) {
            // If the array contains the status, remove
            var index = $scope.filterStatusArray.indexOf(status);
            $scope.filterStatusArray.splice(index, 1);
        }
        else {
            // If it does not contain, add to the array
            $scope.filterStatusArray.push(status);
        }

        socket.emit('findAnalyticsOfProjects', $scope.filterStatusArray);
        analyticsPercentPerOpco()


    }

    // Checks if the filters on, used for styling of buttons
    $scope.checkForFilterOn = function(opco) {
        if (_.contains($scope.filterStatusArray, opco)) {
            return true;
        }


    }
    $scope.analyticsList = [];
    $scope.analyticsAverageList = [];

    $scope.stageProgressionList = [];

    var scoutEntriesData = [];
    var scoutKilledEntriesData = [];
    var scoutCountPerMonth;
    var scoutKilledPerMonth;
    var rateOfInnovation;
    //var trafficSourcesPie;
    var activtyPerOpco;

    $scope.stageAverageList = [];


    setTimeout(tjhAccordion(), 10);
    // Number of new Scout entries per month
    function scoutEntries() {
        var projectList = [];

        angular.copy($scope.projectsList, projectList)

        var currentYear = new Date().getFullYear();

        for (var i = 0; i <= 11; i++) {
            scoutEntriesData[i] = 0;
            scoutKilledEntriesData[i] = 0;

        }

        projectList.forEach(function(project) {
            if (project.stage === 'Scout') {
                if (project.status !== 'Predicted') {
                    var startDate = new Date(project.projectStartDate)
                    if (startDate.getFullYear() === currentYear) {
                        scoutEntriesData[startDate.getMonth()]++;
                        if (project.status === 'Killed') {
                            scoutKilledEntriesData[startDate.getMonth()]++;
                        }
                    }
                }

            }
        });

        scoutEntriesData = ["Scout"].concat(scoutEntriesData)
        scoutKilledEntriesData = ["Scout"].concat(scoutKilledEntriesData)

        scoutCountPerMonth = c3.generate({
            bindto: '#scoutCountPerMonth',
            data: {
                columns: [
                    scoutEntriesData
                ],
                type: 'bar'
            },

            bar: {
                width: {
                    ratio: 0.6 // this makes bar width 50% of length between ticks
                }
            },
            color: {
                pattern: ['#80689D'],
            },
            axis: {
                x: {
                    type: 'category',
                    categories: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
                }
            },
            legend: {
                show: false
            }
            // http://c3js.org/samples/data_color.html
        });
        scoutKilledPerMonth = c3.generate({
            bindto: '#scoutKilledPerMonth',
            data: {
                columns: [
                    scoutKilledEntriesData
                ],
                type: 'bar'
            },

            bar: {
                width: {
                    ratio: 0.6 // this makes bar width 50% of length between ticks
                }
            },
            color: {
                pattern: ['#80689D'],
            },
            axis: {
                x: {
                    type: 'category',
                    categories: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
                }
            },
            legend: {
                show: false
            }
            // http://c3js.org/samples/data_color.html
        });
    }
    $scope.fixScoutCountGraph = function() {
        setTimeout(function() {
            scoutCountPerMonth.flush();
            scoutKilledPerMonth.flush();
        }, 10)
    };

    var checkForProjectsData = setInterval(function() {
        if ($scope.projectsList.length !== 0) {
            scoutEntries();
            clearInterval(checkForProjectsData);
        }
    }, 200);

    /********************** 
     ****Site Analytics ****
     ***********************/

    /************ How many online  *************/


    socket.on('howManyOnlineCountResult', function(number, users) {
        $scope.onlineUsers = users;
        $scope.numberOnline = number;
        $scope.$apply($scope.numberOnline);
        $scope.$apply($scope.onlineUsers);

    });


    /************************ Page Views Per OpCo  ****************/

    var dateFrom = '1015-04-02';
    var dateTo = '3015-04-03';
    var siteStats;

    $('#dateFromAnayltics').datepicker({
        dateFormat: "yy-mm-dd",
        beforeShow: function(input, inst) {
            var maxDate = $('#dateToAnayltics').datepicker('getDate');
            $(this).datepicker('option', 'maxDate', maxDate);
        }
    });

    $('#dateToAnayltics').datepicker({
        dateFormat: "yy-mm-dd",
        beforeShow: function(input, inst) {
            var minDate = $('#dateFromAnayltics').datepicker('getDate');
            $(this).datepicker('option', 'minDate', minDate);
        }
    });

    socket.on('siteStatsPageViewsResult', function(hits, hitOpCos, pages) {

        $scope.viewsByOpco = [];
        for (i = 0; i < hits.length; i++) {

        }

        siteStats.load({
            categories: pages,
            colors: {
                "British Airways": '#021B41',
                Vueling: '#fc0',
                Avios: '#1a4177',
                IAG: '#53565A',
                "IAG Cargo": '#53001b',
                Iberia: '#d7192d'
            }
        });
        siteStats.groups([hitOpCos]);

        hitOpCos.forEach(function(opCo) {
            siteStats.load({
                columns: [
                    [opCo].concat(_.values(hits[opCo]))
                ]

            });
        });
    });

    function generateSiteAnalyticsChart() {
        siteStats = c3.generate({
            bindto: '#siteStats',
            data: {
                columns: [

                ],
                type: 'bar'
            },
            bar: {
                width: {
                    ratio: 0.6 // this makes bar width 50% of length between ticks
                }
            },
            axis: {
                x: {
                    type: 'category',

                }
            }
            // http://c3js.org/samples/data_color.html
        });
    }

    generateSiteAnalyticsChart();
    $scope.refreshSiteAnalyticsPageViews = function() {
        siteStats.destroy();
        socket.emit('siteStatsPageViewsRequest', $scope.dateFromSiteAnalytics, $scope.dateToSiteAnalytics);
        generateSiteAnalyticsChart();
    };

    $scope.resetSiteAnalyticsPageViews = function() {
        $scope.dateFromSiteAnalytics = '';
        $scope.dateToSiteAnalytics = '';
        siteStats.destroy()
        socket.emit('siteStatsPageViewsRequest', '1015-04-02', '3015-04-03');
        generateSiteAnalyticsChart();
    }

    /*  *************** Most active user ********* */
    $scope.popularUserList = [];


    socket.on('siteStatsPopularUserResult', function(data) {

        $scope.popularUserList.push(data);

        $scope.$apply($scope.popularUserList)
    })

    $scope.fixSiteStatsGraph = function() {
        setTimeout(function() {
            siteStats.flush();
        }, 10)
    };

    /* ***************** Traffic Sources *************/


    socket.on('siteStatsTrafficSourcesResult', function(desktopCount, mobileCount) {

        var trafficSourcesPie = c3.generate({
            bindto: '#trafficSourcesPie',
            data: {
                // iris data from R
                columns: [
                    ['Desktop', desktopCount],
                    ['Mobile', mobileCount],
                ],
                type: 'pie',
            }
        });

    });

    /* ************** Activty By OpCo *************** */



    socket.on('siteStatsActivtyPerOpcoResult', function(OpCo) {
        activtyPerOpco.load({
            columns: [OpCo]

        });
    });

    activtyPerOpco = c3.generate({
        bindto: '#activtyPerOpco',
        data: {
            columns: [],
            type: 'pie',
            colors: {
                "British Airways": '#021B41',
                Vueling: '#fc0',
                Avios: '#1a4177',
                IAG: '#53565A',
                "IAG Cargo": '#53001b',
                Iberia: '#d7192d'
            }
        }

        // http://c3js.org/samples/data_color.html
    });



    /* -------------------------- Start of the graphs -------------------------- */


    $scope.fixRateOfInnovation = function() {
        setTimeout(function() {
            rateOfInnovation.flush();
        }, 10)
    };

    var stages = ['Scout', 'Alpha', 'Beta', 'Live'];
    var proStages = ['StoA', 'AtoB', 'BtoL', 'StoL'];



    socket.on('foundAnalyticsOfProjects', function(result) {
        $scope.avgStageLength = {
            Scout: [],
            Alpha: [],
            Beta: [],
            Live: []
        };

        $scope.avgProLength = {
            StoA: [],
            AtoB: [],
            BtoL: [],
            StoL: []
        };

        result.forEach(function(projectG) {

            // sort the project group to make sure it is in correct data
            var projectGroup = _.sortBy(projectG.dates, function(element) {
                var rank = {
                    "Scout": 1,
                    "Alpha": 2,
                    "Beta": 3,
                    "Live": 4
                };
                return rank[element.stage];
            });

            projectGroup.forEach(function(project) {

                //convert to moments, for easy date manipulation
                project.startDate = moment(project.startDate);
                project.endDate = moment(project.endDate);
                //find stage length. start date to end date
                var stageLength = project.endDate.diff(project.startDate, 'days');
                $scope.avgStageLength[project.stage].push(stageLength + 1);

            });



            // find the project stages for progression length
            var projectStages = _.pluck(projectGroup, 'stage');
            if (_.contains(projectStages, 'Scout') && _.contains(projectStages, 'Alpha')) {
                var progressionLength = _.findWhere(projectGroup, {
                    stage: "Alpha"
                }).startDate.diff(_.findWhere(projectGroup, {
                    stage: "Scout"
                }).endDate, 'days');
                console.log(projectGroup, progressionLength + 1)
                $scope.avgProLength.StoA.push(progressionLength + 1)
            }
            if (_.contains(projectStages, 'Alpha') && _.contains(projectStages, 'Beta')) {
                var progressionLength = _.findWhere(projectGroup, {
                    stage: "Beta"
                }).startDate.diff(_.findWhere(projectGroup, {
                    stage: "Alpha"
                }).endDate, 'days');
                $scope.avgProLength.AtoB.push(progressionLength + 1)
            }
            if (_.contains(projectStages, 'Beta') && _.contains(projectStages, 'Live')) {
                var progressionLength = _.findWhere(projectGroup, {
                    stage: "Live"
                }).startDate.diff(_.findWhere(projectGroup, {
                    stage: "Beta"
                }).endDate, 'days');
                $scope.avgProLength.BtoL.push(progressionLength + 1)
            }

            if (_.contains(projectStages, 'Scout') && _.contains(projectStages, 'Live')) {
                var progressionLength = _.findWhere(projectGroup, {
                    stage: "Live"
                }).endDate.diff(_.findWhere(projectGroup, {
                    stage: "Scout"
                }).startDate, 'days');
                                
                console.log(projectGroup, progressionLength + 1)
                
                $scope.avgProLength.StoL.push(progressionLength + 1)
            }



        });

        analyticsFindAvgProLegnth();
        analyticsFindAvgStageLegnth();
        analyticsPercentPerOpco();
        analyticsRatio()
    });

    function analyticsFindAvgStageLegnth() {
        stages.forEach(function(stage) {
            
            var total = 0;
            $scope.avgStageLength[stage].forEach(function(projectLength) {
                console.log(stage)
                total += projectLength;
            });

            $scope.avgStageLength[stage + 'Length'] = total / $scope.avgStageLength[stage].length;
            
        });
    }

    function analyticsFindAvgProLegnth() {
        proStages.forEach(function(stage) {
            var total = 0;
            $scope.avgProLength[stage].forEach(function(projectLength) {
                total += projectLength;
            });
            $scope.avgProLength[stage + 'Length'] = total / $scope.avgProLength[stage].length;
        });

    }


    function analyticsPercentPerOpco() {
        var opcoProjectsPerOpco = [];
        stages.forEach(function(stage) {
            opcoProjectsPerOpco[stage] = [];
            $scope.opcos.forEach(function(opco) {
                var filtered;
                filtered = $filter('selectedOpCoFilter')($scope.projectsList, [opco.name], 'opCo');
                filtered = $filter('selectedFilter')(filtered, $scope.filterStatusArray, 'status');
                filtered = $filter('selectedFilter')(filtered, [stage], 'stage');

                var newEntry = [opco.name, filtered.length];

                opcoProjectsPerOpco[stage].push(newEntry);
            });


            var chartstage = c3.generate({
                bindto: '#stagePercentPie' + stage,
                size: {
                    height: 300,
                    width: 400
                },
                data: {
                    columns: opcoProjectsPerOpco[stage],
                    type: 'pie',
                    colors: {
                        "Aer Lingus": '#008374',
                        "British Airways": '#021B41',
                        Vueling: '#fc0',
                        Avios: '#1a4177',
                        IAG: '#53565A',
                        "IAG Cargo": '#53001b',
                        Iberia: '#d7192d'
                    }

                }
            });

        });
    }

    function analyticsRatio() {
        $scope.analyticsRatio = {};
        stages.forEach(function(stage) {
            var filtered;
            filtered = $filter('selectedFilter')($scope.projectsList, [stage], 'stage');
            filtered = $filter('selectedFilter')(filtered, $scope.filterStatusArray, 'status');

            $scope.analyticsRatio[stage] = filtered.length;
        });
    }


});