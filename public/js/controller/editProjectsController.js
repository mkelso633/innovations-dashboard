var editProjectsController = angular.module('editProjectsController', []);

editProjectsController.controller('EditProjectsController', function($scope, $location) {

    $scope.editProjectList = [];
    $scope.example1model = [];

    function checkAccess() {
        if ($scope.access !== undefined) {
            accessLoaded();
            clearInterval(checkInterval);
        }
    }
    checkAccess();

    var checkInterval = setInterval(function() {
        checkAccess();
    }, 200);

    function projectListLoaded() {
        var findArray = findAllInArray($scope.projectsList, 'reference', $scope.projectRefProjectRef);
        $scope.opcosMultiSelect = angular.copy($scope.opcos);
        
        if (findArray[0].opCo.constructor !== Array) {
            // this is to fix the old data to bring it into the new array format to support multi opco
            findArray.forEach(function(project) {
                project.opcosMultiSelect = angular.copy($scope.opcos)
                project.opcosMultiSelect.forEach(function(opco) {
                        if (project.opCo === opco.name) {
                            opco.ticked = true;
                        }
                    });
            });
        }
        else {
            // #dont judge
            findArray.forEach(function(project) {
                project.opcosMultiSelect = angular.copy($scope.opcos)
                project.opCo.forEach(function(selectedOpCo) {
                    project.opcosMultiSelect.forEach(function(opco) {
                        if (selectedOpCo.name === opco.name) {
                            opco.ticked = true;
                        }
                    });
                });
            });
        }

        $scope.editProjectList = angular.copy(findArray);
        $scope.$apply($scope.editProjectList);
    }

    function accessLoaded() {
        if ($scope.access <= 3) {
            $location.path('home');
        }
        else {
            var pageURL = $location.url();

            if (pageURL.substring(1, 5) === 'edit') {
                $scope.pageTitle = 'Edit Mode'; // if you change this then edit the save function
                var projectRef = pageURL.substring(6, pageURL.length);
                $scope.projectRefProjectRef = projectRef;

                /* checkProjectListLoaded Interval is to fix a bug. without this you cannot refresh on edit mode*/
                function checkProjectListLoaded() {
                    if ($scope.projectsList.length !== 0) {
                        projectListLoaded();
                        clearInterval(checkProjectListInterval);
                    }
                };

                var checkProjectListInterval = setInterval(function() {
                    checkProjectListLoaded();
                }, 200);


            }
            else if (pageURL.substring(1, 4) === 'add') {
                $scope.pageTitle = 'Add New Project'; // if you change this then edit the save function
                $scope.editProjectList = [];

                var newObj = {
                    _id: (1e4 * (Date.now() + Math.random())).toString(16),
                    reference: (1e4 * (Date.now() + Math.random())).toString(16),
                    stage: '',
                    ragStatus: 'Green',
                    Component: [{
                        _id: (1e4 * (Date.now() + Math.random())).toString(16),
                        componentName: "",
                        startDate: "",
                        endDate: "",
                        percentage: ""
                    }],
                    opcosMultiSelect: angular.copy($scope.opcos)
                };
                $scope.editProjectList.push(newObj);
            }
            setTimeout(function() {
                $scope.$apply($scope.editProjectList)
            }, 10)


        }


        setTimeout(function() {
            $(document).foundation('reveal', 'reflow');
        }, 200);


    }


    $scope.checkDates = function(componentId) {


        $('#startDate-' + componentId).datepicker({
            dateFormat: "dd/mm/yy",

            beforeShow: function(input, inst) {
                var maxDate = $('#endDate-' + componentId).datepicker('getDate');
                $(this).datepicker('option', 'maxDate', maxDate);
            }
        });


        $('#endDate-' + componentId).datepicker({
            dateFormat: "dd/mm/yy",
            beforeShow: function(input, inst) {

                var minDate = $('#startDate-' + componentId).datepicker('getDate');
                $(this).datepicker('option', 'minDate', minDate);

            }
        });

    }





    function foundError(id, projectID) {
        var field = document.getElementById(id)
        field.className += " invalidField";
        var projectPosition = $("#project-" + projectID).offset().top
        $('html, body').animate({
            scrollTop: projectPosition - 80
        }, 200); //reduced to 200 for from 2000
    }

    function removeError(id) {
        var field = document.getElementById(id)
        field.classList.remove("invalidField");
    }

    $scope.saveAndClose = function() {
        var error = false;
        var requireArray = ['title', 'type', 'status', 'stage']
        $scope.editProjectList.forEach(function(project) {
                // Adds http:// to the start of the url if it is not there
            project.oneNote = httpData(project.oneNote)
            project.document1Link = httpData(project.document1Link)
            project.document2Link = httpData(project.document2Link)

            // alert to tell user that they need to have a component because of the date
            if (project.Component[0] === undefined) {
                alert('Each project must have at least one component')
                error = true;
            }
            else {
                //takes the date and converts it to a json date to allow sorting
                var day = project.Component[0].startDate.substring(0, 2);
                var month = project.Component[0].startDate.substring(3, 5);
                var year = project.Component[0].startDate.substring(6, 10);
                project.projectStartDate = year.concat('-').concat(month).concat('-').concat(day).concat('T00:00:00.000Z');


                // calculate the latest end date of the project
                var day = project.Component[project.Component.length - 1].endDate.substring(0, 2);
                var month = project.Component[project.Component.length - 1].endDate.substring(3, 5);
                var year = project.Component[project.Component.length - 1].endDate.substring(6, 10);
                project.projectEndDate = year.concat('-').concat(month).concat('-').concat(day).concat('T00:00:00.000Z');

                //checks to make sure that the date field is filled out
                if (project.Component[0].startDate === '') {
                    foundError("startDate-" + project.Component[0]._id, project._id);
                    error = true;
                }
                else {
                    removeError("startDate-" + project.Component[0]._id);
                }
                if (project.Component[project.Component.length - 1].endDate === '') {
                    foundError("endDate-" + project.Component[project.Component.length - 1]._id, project._id);
                    error = true;
                }
                else {
                    removeError("endDate-" + project.Component[project.Component.length - 1]._id);
                }
            }

            // goes through the array at the start of this function to check if the fields contain data
            requireArray.forEach(function(field) {

                if (project[field] === '' || project[field] === undefined) {
                    foundError(field + "-" + project._id, project._id); //puts a red box around the field
                    error = true;
                }
                else {
                    removeError(field + "-" + project._id); // removes the red box from the field
                }
            });
            // checks if the percentage has a number in it

            //proceed with rest of code

            project.Component.forEach(function(component) {

                // check to make sure not empty and only contains numbers
                if (component.percentage === '' || component.percentage === undefined || !(/^\d+$/.test(component.percentage))) {
                    component.percentage = 100;
                    //foundError("compPercent-" + component._id, project._id);
                    //error = true;
                }
                else {
                    //removeError("compPercent-" + component._id);
                    //check to make sure it is between 0 and 100 
                    var percentageINT = parseInt(component.percentage)
                    if (percentageINT < 0 || percentageINT > 100) {
                        foundError("compPercent-" + component._id, project._id);
                        error = true;
                    }
                    else {
                        removeError("compPercent-" + component._id);
                    }
                }
            })

            // used to make sure the timeline is closed by defualt. 
            if (project.TimelineVisible === true) {
                project.TimelineVisible = false;
            }

            /// here tom

        }); // end of forEach Loop

        // if there are no errors it saves client side and server side
        if (!error) {
            $scope.editProjectList.forEach(function(project) {
                delete project['opcosMultiSelect'];
            });
            if ($scope.pageTitle === 'Add New Project') {
                socket.emit('saveProject', $scope.editProjectList, $scope.editProjectList[0].reference);
                $scope.$emit('savedData', $scope.editProjectList, $scope.editProjectList[0].reference);
            }
            else if ($scope.pageTitle === 'Edit Mode') {
                socket.emit('saveProject', $scope.editProjectList, $scope.projectRefProjectRef);
                $scope.$emit('savedData', $scope.editProjectList, $scope.projectRefProjectRef);
            }
            window.history.back();
        }
    };

    $scope.cancel = function() {
        window.history.back();
    };

    $scope.addNewStage = function() {
        var relatedProject = $scope.editProjectList[0];
        var newObj = {
            _id: (1e4 * (Date.now() + Math.random())).toString(16),
            reference: relatedProject.reference,
            opcosMultiSelect: angular.copy($scope.opcos),
            title: relatedProject.title,
            description: relatedProject.description,
            stage: '',
            oneNote: relatedProject.oneNote,
            document1Title: relatedProject.document1Title,
            document2Title: relatedProject.document2Title,
            document1Link: relatedProject.document1Link,
            document2Link: relatedProject.document2Link,
            ragStatus: 'Green',
            Component: [{
                _id: (1e4 * (Date.now() + Math.random())).toString(16),
                componentName: "",
                startDate: "",
                endDate: "",
                percentage: ""
            }]
        };
        $scope.editProjectList.push(newObj);

        /*setTimeout(function() {
            $(".datepicker").datepicker({
                dateFormat: "dd/mm/yy"
            });
        }, 50);*/
    };


    $scope.addNewComponent = function(project) {
        var newObj = {
            _id: (1e4 * (Date.now() + Math.random())).toString(16),
            componentName: "",
            startDate: "",
            endDate: "",
            percentage: ""
        }

        project.Component.push(newObj)
            /*setTimeout(function() {
                $(".datepicker").datepicker({
                    dateFormat: "dd/mm/yy"
                });
            }, 50);*/
    };

    $scope.deleteComponent = function(project, id) {
        project.Component = removeFunction(project.Component, '_id', id);
    };

    // remove the project
    $scope.deleteProject = function(id) {
        $scope.editProjectList = removeFunction($scope.editProjectList, '_id', id);
        $('#confirmDelete').foundation('reveal', 'close');
    };

    // ask if they are sure if they want to delete project
    $scope.confirmDeleteProject = function(projectID) {
        $('#confirmDelete').foundation('reveal', 'open');
        $scope.projectToDelete = projectID;
    }
    $scope.closeDeleteModal = function() {
        $('#confirmDelete').foundation('reveal', 'close');
    }

});