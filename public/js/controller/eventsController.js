var eventsController = angular.module('eventsController', []);

eventsController.controller('EventsController', function($scope, uiCalendarConfig, $document, $filter) {

    function onLoad() {
        if ($scope.clickedEvent === undefined) {
            // set clickedEvent to have the current/next upcoming event by default
            // filter to get only events starting today/after today
            $scope.clickedEvent = $filter('eventCarouselFilter')($scope.eventsList);

            // order by start date, earliest first
            $scope.clickedEvent = $filter('orderBy')($scope.clickedEvent, 'start', false);

            // take the first object in the array
            angular.copy($scope.clickedEvent[0], $scope.clickedEvent);

            $scope.clickedEvent.start = moment($scope.clickedEvent.start);
            $scope.clickedEvent.end = moment($scope.clickedEvent.end);
            $scope.$apply($scope.clickedEvent)
        }
    }

    var checkForEventsData = setInterval(function() {
        if ($scope.eventsList.length !== 0) {
            onLoad();
            clearInterval(checkForEventsData);
        }
    }, 200);

    $document.ready(function() {
        // Initialise dateFrom picker, if date has been chosen in dateTo, dateFrom cannot be after dateTo
        $('#dateFrom').datepicker({
            dateFormat: "yy-mm-dd",
            beforeShow: function(input, inst) {
                var maxDate = $('#dateTo').datepicker('getDate');
                $(this).datepicker('option', 'maxDate', maxDate);
            }
        });
        // Initialise dateTo picker, if date has been chosen in datefrom, dateTo cannot be before dateFrom
        $('#dateTo').datepicker({
            dateFormat: "yy-mm-dd",
            beforeShow: function(input, inst) {
                var minDate = $('#dateFrom').datepicker('getDate');
                $(this).datepicker('option', 'minDate', minDate);
            }
        });

        //Initialise timepickerStart
        $('#timepickerStart').timepicker({
            step: 30,
            timeFormat: 'H:i:s',
            show: 2400,
        });

        //Initialise timepickerEnd
        $('#timepickerEnd').timepicker({
            step: 30,
            timeFormat: 'H:i:s',
            show: 2400,
        });

    });


    $scope.eventTitle;
    $scope.eventDescription;

    $scope.datePickerStart;
    $scope.timePickerStart;

    $scope.datePickerEnd;
    $scope.timePickerEnd;

    // Add a custom event
    $scope.addEvent = function() {

        $scope.addEventError = false;

        console.log('allDay? ', $scope.allDay);
        var newEvent = {
            _id: (1e4 * (Date.now() + Math.random())).toString(16),
            title: $scope.eventTitle,
            allDay: $scope.allDay,
            description: $scope.eventDescription,
            className: $scope.eventCategory,
            creator: $scope.fullName
        }

        if ($scope.allDay) {
            newEvent.start = $scope.datePickerStart;
            newEvent.end = moment($scope.datePickerEnd).add(1, 'days').format('YYYY-MM-DD'); // add 1 day
            newEvent.displayEndDate = moment($scope.datePickerEnd).format();

            if ($scope.datePickerStart == $scope.datePickerEnd) {
                newEvent.sameDay = true;
            }

            if ($scope.datePickerStart !== undefined && $scope.datePickerEnd !== undefined) {
                if ($scope.datePickerStart.length == 10 && $scope.datePickerEnd.length == 10) {}
                else {
                    $scope.addEventError = true;
                    return false;
                }
            }
            else {
                $scope.addEventError = true;
                return false;
            }
        }
        else {
            newEvent.start = $scope.datePickerStart + "T" + $scope.timePickerStart;
            newEvent.end = $scope.datePickerEnd + "T" + $scope.timePickerEnd;

            if ($scope.datePickerStart !== undefined && $scope.timePickerStart !== undefined && $scope.datePickerEnd !== undefined && $scope.timePickerEnd !== undefined) {
                if ($scope.datePickerStart.length == 10 && $scope.timePickerStart.length == 8 && $scope.datePickerEnd.length == 10 && $scope.timePickerEnd.length == 8) {}
                else {
                    $scope.addEventError = true;
                    return false;
                }
            }
            else {
                $scope.addEventError = true;
                return false;
            }
        }

        // Push the new event to an array
        $scope.eventsList.push(newEvent);

        socket.emit('addEvent', newEvent)

        $scope.eventTitle = '';
        $scope.datePickerStart = '';
        $scope.timePickerStart = '';
        $scope.datePickerEnd = '';
        $scope.timePickerEnd = '';
        $scope.eventDescription = '';
        $scope.eventCategory = '';
        $scope.allDay = false;



    };

    // Open confirm delete modal
    $scope.confirmDeleteEvent = function(event) {
        $('#confirmDelete').foundation('reveal', 'open');
    }

    // Delete an event from the database
    $scope.deleteEvent = function(event) {

        var index = findIndexByKeyValue($scope.eventsList, '_id', event._id);
        $scope.eventsList.splice(index, 1);

        socket.emit('deleteEvent', event._id);
        $scope.clickedEvent = {};
        $scope.clickedEvent.title = '';

        $('#confirmDelete').foundation('reveal', 'close');
    };

    // Close the delete modal
    $scope.closeDeleteModal = function() {
        $('#confirmDelete').foundation('reveal', 'close');
    }

    // Edit an event
    $scope.editEventModalOpen = function(event) {
        $scope.editEvent = {};

        // Open modal
        $('#editEventModal').foundation('reveal', 'open');

        // Initialise datepicker
        $('#editDateFrom').datepicker({
            dateFormat: "yy-mm-dd",
            beforeShow: function(input, inst) {
                var maxDate = $('#editDateTo').datepicker('getDate');
                $(this).datepicker('option', 'maxDate', maxDate);
            }
        });

        // Initialise datepicker
        $('#editDateTo').datepicker({
            dateFormat: "yy-mm-dd",
            beforeShow: function(input, inst) {
                var minDate = $('#editDateFrom').datepicker('getDate');
                $(this).datepicker('option', 'minDate', minDate);
            }
        });

        // Initialise timepicker
        $('#editTimepickerStart').timepicker({
            step: 30,
            timeFormat: 'H:i:s',
            show: 2400,
        });

        // Initialise timepicker
        $('#editTimepickerEnd').timepicker({
            step: 30,
            timeFormat: 'H:i:s',
            show: 2400,
        });

        // Copy the editEvent array
        angular.copy(event, $scope.editEvent)

        $scope.editEvent.category = event.className[0]

        if (!event.allDay) {
            $scope.editEvent.startDate = event.start._i.substring(0, 10);
            $scope.editEvent.startTime = event.start._i.substring(11, 19);

            if ($scope.editEvent.end === null) {
                $scope.editEvent.endDate = $scope.editEvent.startDate
                $scope.editEvent.endTime = $scope.editEvent.startTime
            }
            else {
                $scope.editEvent.endDate = event.end._i.substring(0, 10);
                $scope.editEvent.endTime = event.end._i.substring(11, 19);
            }
        }
        else {
            // all day events
            $scope.editEvent.startDate = event.start._i.substring(0, 10);
            $scope.editEvent.endDate = event.displayEndDate.substring(0, 10);

        }



    };

    $scope.updateEvent = function(editEvent) {

        $scope.editEventError = false;

        var manipulatedEvent = {};

        manipulatedEvent._id = editEvent._id;
        manipulatedEvent.title = editEvent.title;
        manipulatedEvent.description = editEvent.description;
        manipulatedEvent.className = editEvent.category;
        manipulatedEvent.allDay = editEvent.allDay;


        if (editEvent.allDay) {
            manipulatedEvent.start = editEvent.startDate
            manipulatedEvent.end = moment(editEvent.endDate).add(1, 'days').format('YYYY-MM-DD');
            manipulatedEvent.displayEndDate = moment(editEvent.endDate).format();


            if (editEvent.startDate !== undefined && editEvent.endDate !== undefined) {
                if (editEvent.startDate.length == 10 && editEvent.endDate.length == 10) {}
                else {
                    $scope.addEventError = true;
                    return false;
                }
            }
            else {
                $scope.addEventError = true;
                return false;
            }


        }
        else {
            manipulatedEvent.start = editEvent.startDate + 'T' + editEvent.startTime;
            manipulatedEvent.end = editEvent.endDate + 'T' + editEvent.endTime;
            if (editEvent.startDate !== undefined && editEvent.startTime !== undefined && editEvent.endDate !== undefined && editEvent.endTime !== undefined) {
                if (editEvent.startDate.length == 10 && editEvent.startTime.length == 8 && editEvent.endDate.length == 10 && editEvent.endTime.length == 8) {}
                else {
                    $scope.editEventError = true;
                    return false;
                }
            }
            else {
                $scope.editEventError = true;
                return false;
            }
        }

        socket.emit('editEvent', manipulatedEvent)
        console.log(manipulatedEvent)
            //$scope.eventsList = removeFunction($scope.eventsList, '_id', manipulatedEvent._id);


        $scope.editMode = false;
        $('#editEventModal').foundation('reveal', 'close');

        $scope.$emit('updateEventsData', manipulatedEvent, manipulatedEvent._id);


        $scope.eventSources.splice(0, $scope.eventSources.length);
        $scope.eventSources.push($scope.eventsList)
        $('#calendar').fullCalendar('addEventSource', $scope.eventSources)

        $('#editEventModal').foundation('reveal', 'close');

    }


    /* Change View */

    $scope.renderCalender = function(calendar) {
        if (uiCalendarConfig.calendars[calendar]) {
            uiCalendarConfig.calendars[calendar].fullCalendar('render');
        }
    };


    $scope.changeView = function(view, calendar) {
        uiCalendarConfig.calendars[calendar].fullCalendar('changeView', view);
    };

    /* alert on eventClick */
    $scope.alertOnEventClick = function(data, jsEvent, view) {
        $scope.editMode = true;
        $scope.clickedEvent = data;
    };

    /* config object */

    $scope.uiConfig = {
        calendar: {
            height: 800,
            fixedWeekCount: false,
            editable: false, // was true
            header: {
                left: 'year month agendaWeek agendaDay',
                center: 'title',
                right: 'today prev,next',
            },
            eventClick: $scope.alertOnEventClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            eventRender: $scope.eventRender
        }
    };



    $scope.selectedCategory = 'ALL'
    $scope.filterCatsArray = ['cat-leave', 'cat-dbt', 'cat-deadline', 'cat-stakeholder', 'cat-conf', 'cat-internal'];

    // event category filter 
    $scope.selectEventsFilter = function(cat) {

        if (_.contains($scope.filterCatsArray, cat)) {
            // If the array contains the status, remove
            var index = $scope.filterCatsArray.indexOf(cat);
            $scope.filterCatsArray.splice(index, 1);
        }
        else {
            // If it does not contain, add to the array
            $scope.filterCatsArray.push(cat);
        }

        $scope.filteredEventsList = $filter('selectedFilter')($scope.eventsList, $scope.filterCatsArray, 'className');
/*
        if (event === $scope.selectedCategory) {
            $scope.filteredEventsList = $scope.eventsList;
            $scope.selectedCategory = 'ALL';
        }
        else {
            $scope.selectedCategory = event;
        }*/

        console.log($scope.filteredEventsList)
        $scope.eventSources.splice(0, $scope.eventSources.length);
        $scope.eventSources.push($scope.filteredEventsList)
        $('#calendar').fullCalendar('addEventSource', $scope.eventSources)


    }

    $scope.checkForFilterOn = function(cat) {

        if (_.contains($scope.filterCatsArray, cat)) {
            return true;
        }


    }


});