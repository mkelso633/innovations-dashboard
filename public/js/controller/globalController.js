var globalController = angular.module('globalController', []);
globalController.controller('GlobalController', function($scope, $location, $filter) {

    $scope.version = '1.1.0';

    // Arrays of objects used for multi-opco projects
    $scope.opcos = [{
        name: 'Aer Lingus',
        shortName: 'AL'
    }, {
        name: 'Avios',
        shortName: 'AV'
    }, {
        name: 'British Airways',
        shortName: 'BA'
    }, {
        name: 'IAG',
        shortName: 'IAG'
    }, {
        name: 'IAG Cargo',
        shortName: 'IAG CA'
    }, {
        name: 'Iberia',
        shortName: 'IB'
    }, {
        name: 'Vueling',
        shortName: 'VU'
    }];

    // Array of types
    $scope.types = ['Contact', 'Triage', 'DesignThinking', 'Project', 'FastPrototype', 'Catapult', 'OpCo'];

    // Array of stages
    $scope.stages = ['Scout', 'Alpha', 'Beta', 'Live', 'Other'];

    // Array of rag statuses
    $scope.rags = ['Red', 'Amber', 'Green'];

    // Array of objects for Categories
    $scope.categories = [{
        name: 'Agile',
        title: 'Agile and flexible organisation to drive innovation'
    }, {
        name: 'CI',
        title: 'CI & Customer centric approach'
    }, {
        name: 'Sales',
        title: 'Profitable Sales'
    }, {
        name: 'Experience',
        title: 'Customer Experience'
    }, {
        name: 'Operational',
        title: 'Operational Efficiency'
    }];

    // Array of statuses
    $scope.statuses = ['Active', 'Progressed', 'Killed', 'Predicted'];
    $scope.SABLO = {};

    // Array lists for dashboard defined
    $scope.projectsList = [];
    $scope.newsList = [];
    $scope.eventsList = [];
    $scope.eventSources = [];

    // Get todays date
    var todaysDate = moment();

    // New article has not been added 
    var newArticle = false;

    // Initialise foundation components
    $(document).foundation('alert', 'events');
    $(document).foundation('equalizer', 'reflow');

    // User has been found in the database. Login Method is noted.
    socket.on('foundUser', function(access, fullName, loginMethod) {
        $scope.access = access;
        $scope.$apply($scope.access);

        $scope.fullName = fullName;

        if (loginMethod !== 'bSafe') {
            $scope.showChangeMyPass = true;
        }
    });

    // Once logged in, if the user is new. They are prompted with a modal to enter more information.
    socket.on('NewUser', function() {
        $('#newUserModal').foundation('reveal', 'open');
    });

    // Page counts for homepage. Displayed on the tiles.
    socket.on('homepageNumberResult', function(scoutCount, alphaCount, betaCount, liveCount) {
        $scope.SABLO.Scout = scoutCount;
        $scope.SABLO.Alpha = alphaCount;
        $scope.SABLO.Beta = betaCount;
        $scope.SABLO.Live = liveCount;

        $scope.$apply($scope.SABLO)
    });

    // Loads the projectList
    socket.on('projectsListLoad', function(data) {

        $scope.projectsList = data

        // Only used by Analytics

        data.forEach(function(project) {
            var projectStartDate = moment(project.projectStartDate);
            var projectEndDate = moment(project.projectEndDate);

            project.projectLength = projectEndDate.diff(projectStartDate, 'days') + 1

        });
        $scope.$apply($scope.projectsList);
    });

    // Loads the event List
    socket.on('eventsListLoad', function(data) {

        if ($scope.eventsList.length === 0) {
            //$scope.eventsList.splice(0, $scope.eventsList.length);
            //$scope.eventSources.splice(0, $scope.eventSources.length);

            data.forEach(function(event) {
                $scope.eventsList.push(event)
            });

            $scope.eventSources.push($scope.eventsList);
            $('#calendar').fullCalendar('rerenderEvents')
            $scope.$apply()
        }
    });

    // Loads all news (wire and seismograph)
    socket.on('allNewsLoad', function(data) {

        if ($scope.newsList.length === 0) {
            $scope.newsList = data;
            // for each news item in newsList
            $scope.newsList.forEach(function(newsItem) {

                //find the dashboard video guide
                if (newsItem.title === 'Dashboard Video Guide') {
                    if (newsItem.videoURL !== undefined) {
                        var videoPlayer = document.getElementById('dashboard-video-guide');
                        videoPlayer.innerHTML = '<video width="640" height="480" controls preload="metadata" id="dashboard-video"> <source src="/images/uploads/' + newsItem.videoURL + '"type="video/mp4"> </video> ';
                    }
                }

                // find position of </p>
                var paraPosStart = newsItem.body.indexOf("<p");
                var paraPosEnd = newsItem.body.indexOf("</p>");
                // substring news content from start to </p>
                // create new variable from substring

                if (paraPosStart > -1) {
                    paraPosStart = newsItem.body.indexOf(">") + 1;
                    newsItem.firstParagraph = newsItem.body.substring(paraPosStart, paraPosEnd);
                }
                else {
                    newsItem.firstParagraph = "We cannot show a preview of this news item.";
                }

                if (newsItem.page == "Wire") {
                    var wireDate = moment(newsItem.date);

                    var difference = wireDate.diff(todaysDate, 'days')

                    if (difference <= 14 && difference >= -14) {
                        newArticle = true;
                        newsItem.newArticle = newArticle;

                    }

                }

            });
            $scope.newsList = $filter('orderBy')($scope.newsList, 'date', true)
            $scope.$apply($scope.newsList);
            $scope.$apply($scope.article);

        }
    });

    // Update edited project data
    function updateData(editedList, idToRemove, scope, ref) {
        console.log(editedList)
        $scope[scope] = _.reject($scope[scope], function(el) {
            return el[ref] == idToRemove;
        });

        $scope[scope] = $scope[scope].concat(editedList);

    }


    $scope.$on('savedData', function(scopeInfo, editedList, projectRef) {
        updateData(editedList, projectRef, 'projectsList', 'reference');
    });

    // project data
    socket.on('updateData', function(editedList, projectRef) {
        updateData(editedList, projectRef, 'projectsList', 'reference');
        $scope.$apply($scope.projectsList)
    });

    $scope.$on('updateEventsData', function(scopeInfo, event, eventRef) {
        updateData(event, eventRef, 'eventsList', '_id');
    });



    // Slider is reinitalised
    $scope.fixSlider = function() {
        setTimeout(function() {
            $(document).foundation('slider', 'reflow');
        }, 600);
    }

    // To check what device the user is using. As certain functions should only happen on certain devices.
    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };
    if (isMobile.any()) {
        $scope.mobile = true;
    }

    // Opens the event page
    $scope.openEventPage = function(event) {
        $scope.clickedEvent = {}
        angular.copy(event, $scope.clickedEvent);
        $scope.clickedEvent.start = moment($scope.clickedEvent.start);
        $scope.clickedEvent.end = moment($scope.clickedEvent.end);
        $location.path('/Events')
    }

    function siteStats() {
        var page = $location.path().substr(1);
        if (page.substring(0, 9) === 'projects/') {
            page = page.substring(9)
        }
        else if (page.substring(0, 5) === 'edit/') {
            page = 'Edit'
        }
        else if (page.substring(0, 8) === 'article/') {
            page = 'Article'
        }

        if (page === '') {
            page = 'Home'
        }

        var mobile = $scope.mobile;
        if (mobile === undefined)
            mobile = false;

        page = capitalizeFirstLetter(page)

        socket.emit('siteStatsInsert', page, mobile);


    }

    // Login error user is redirected to the signout page.
    socket.on('loginError', function() {
        document.location.href = '/signout';
    });

    // Changes the users location.
    $scope.$on('$locationChangeSuccess', function(event) {
        if ($location.path() === '/projects/Scout' || $location.path() === '/projects/Alpha' || $location.path() === '/projects/Beta' || $location.path() === '/projects/Live' || $location.path() === '/projects/Other' || $location.path() === '/stakeholder') {
            $scope.hideFilterButton = false;
        }
        else {
            $scope.hideFilterButton = true;
        }

        if ($scope.access !== undefined) {
            siteStats();
        }


    });

    //Send the user to on the wire from the help modal
    $scope.goToOnTheWire = function() {
        $('#helpModal').foundation('reveal', 'close');
        $location.path('/Wire');
    }

    $scope.goldenRulesModal = function(title) {
        if (title == "Cloud First") {
            $scope.title = title;
            $scope.description = "IAG Digital has moved the emphasis from Why the Cloud? to “Why not the Cloud?” This means that public cloud services should be considered for deployment within IAG as the first choice wherever possible."
        }
        if (title == "Identity and entitlement as a service") {
            $scope.title = title;
            $scope.description = "Everyone's identity across the group should be recognised for any services offered across the IAG operating companies, without having to duplicate a new identity. Entitlement as a service implies that business capabilities should all be enabled by default in all Software as a Service (SaaS) systems."
        }
        if (title == "Expose your data") {
            $scope.title = title;
            $scope.description = "Exposing your data should be included within the prioritised requirements list (PRL) for all new systems, services and business capabilities."
        }
        if (title == "Maintain a headless (API) architecture") {
            $scope.title = title;
            $scope.description = "To enable speed to market and richer customer experiences, all systems, services and business capabilities should use a headless architecture by default. This separates the customer-facing user interface from the systems themselves, allowing for more flexible and user-friendly design customer experiences."
        }
        if (title == "Treat every pixel as an interactive pixel") {
            $scope.title = title;
            $scope.description = "Business capabilities should be designed to be able to provide the same service through any front end. For example, providing a way finding service by using a range of one pixel (e.g. an LED light) to 10 billion pixels (e.g. a large screen display in an airport terminal). This means the only difference is in the customer interface, and the heavy lifting of building the service is not duplicated."
        }
        if (title == "Services and systems must only communicate through web services") {
            $scope.title = title;
            $scope.description = "As per the headless architecture rule, intercommunication of business capabilities should be done through web service calls and not through proprietary messaging formats."
        }
        if (title == "Services must all be externalisable") {
            $scope.title = title;
            $scope.description = "All service interfaces must be designed from the ground up to be externalisable. This means all interfaces must be planned and designed to be able to be exposed to developers in the outside world through APIs."
        }
        if (title == "Snap back to a high efficiency model") {
            $scope.title = title;
            $scope.description = "Look at a business problem starting with a minimum viable product, and only add complexity at later stages where it makes sense."
        }
        if (title == "Set transformational targets") {
            $scope.title = title;
            $scope.description = "This is also known as the power of 100% and the power of 0%. If you set transformational targets for 100% adoption, there is nowhere to hide. When you set a 95% adoption target, many parts of the business will assign themselves to the 5% exception. Even if you never actually get to 100% adoption, you’ll always get a larger take up than if you set a non-transformation target."
        }


    }

});