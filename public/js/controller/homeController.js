var homeController = angular.module('homeController', []);

homeController.controller('HomeController', function($scope, $location) {

    $scope.newArticle = false;
    $scope.newArticleFound = false;


    // Initialises modal
    setTimeout(function() {
        $(document).foundation('reveal', 'reflow');
    }, 30);


    // Loads the event carousel with specific settings
    function eventsCarouselLoad() {
        $("#events-carousel").owlCarousel({
            navigation: false,
            singleItem: true,
            transitionStyle: "goDown",
            mouseDrag: false,
            slideSpeed: 900,
            autoPlay: 7000,

        });
    }

    // Loads the seismograph carousel with specific settings
    function newsCarouselLoad() {
        $("#blogs-carousel").owlCarousel({
            navigation: false,
            slideSpeed: 900,
            singleItem: true,
            autoPlay: 8000,
            mouseDrag: false
        });
    }

    var checkCarousels = setInterval(function() {
        newsCarouselLoad();
        eventsCarouselLoad();
    }, 30000);

    // Waits to see if the eventsList has been loaded. Then loads the event carousel.
    function checkEvents() {
        if ($scope.eventsList.length !== 0) {
            eventsCarouselLoad();
            clearInterval(checkEventsInterval);
        }
    }

    var checkEventsInterval = setInterval(function() {
        checkEvents();
    }, 200);

    function checkNews() {
        if ($scope.newsList.length !== 0) {
            newsCarouselLoad();
            $scope.checkNewArticle();
            clearInterval(checkNewsInterval);
        }
    }

    var checkNewsInterval = setInterval(function() {
        checkNews();
    }, 200);

    // Automatically adds the article thumbnail to the news page
    $scope.checkNewArticle = function() {
        $scope.newArticleFound = true;
        $scope.newsList.forEach(function(item) {
            if (item.newArticle == true) {
                $scope.newArticle = true;
                $scope.$apply($scope.newArticle)
            }
        });
    }
    


  


});
