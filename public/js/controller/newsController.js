var newsController = angular.module('newsController', []);

newsController.controller('NewsController', function($scope, $location) {

    $scope.showPercentUploading = false;
    $scope.pageSize = 3;

    // Reads the url to see if the news page should use the seismograph or on the wire code.
    $scope.pageURL = $location.path().substring(1, $location.path().length)

    $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false
    };

    setTimeout(function() {
        $("#dateOfNews").datepicker({
            dateFormat: "dd/mm/yy",
        });
        $(document).foundation('reveal', 'reflow');
    }, 200)

    // To load the full article from the thumbnail on the news' pages.
    function loadArticle(articleID) {
        $scope.newsList.forEach(function(news) {
            if (news._id === articleID) {
                $scope.article = news;
                if ($scope.article.videoURL !== '' && $scope.article.videoURL !== undefined) {
                    var videoURL = $scope.article.videoURL;
                    var videoPlayer = document.getElementById('video-test');

                    videoPlayer.innerHTML = '<video width="640" height="480" controls preload="auto"> <source src="/images/uploads/' + videoURL + '"type="video/mp4"> </video> ';


                }
                $scope.$apply($scope.article);
            }
        });
    }

    var pageURL = $location.url();
    if (pageURL.substring(1, 8) === 'article') {
        var articleID = pageURL.substring(9, pageURL.length)

        var checkForNewsData = setInterval(function() {
            if ($scope.newsList.length !== 0) {
                loadArticle(articleID);
                clearInterval(checkForNewsData);

            }
        }, 200);

    }
    else if (pageURL.substring(1, 5) === 'news') {


        //uploader.listenOnSubmit(document.getElementById("addArticleSave"), document.getElementById("siofu_input_video"));
    }

    // Add an article
    $scope.addNewsButton = function() {
        var todaysDate = moment().format('DD/MM/YYYY');
        $scope.newNews = {
            selectDate: todaysDate
        }

        document.getElementById('addArticleSave').innerHTML = 'Save';

    }

    // Close the custom delete article modal
    $scope.closeDeleteModal = function() {
        $('#confirmDelete').foundation('reveal', 'close');
    }

    // Close the custom add article modal
    function closeAddModal() {
        $("#addNews").toggleClass("closed");
        $("#curtain").fadeOut(100);
        $scope.newNews = {};
        $('input').val("");
    }

    $scope.toggleAddNewsModal = function() {
        closeAddModal();
    }

    // Add article function with validation. User must enter required fields and have a image uploaded. If requirements are met, then the article is uploaded to the database
    $scope.addArticleSave = function() {

        var error = false;

        var day = $scope.newNews.selectDate.substring(0, 2);
        var month = $scope.newNews.selectDate.substring(3, 5);
        var year = $scope.newNews.selectDate.substring(6, 10);
        var fullDate = month.concat('/').concat(day).concat('/').concat(year);
        $scope.newNews.date = new Date(fullDate);

        if ($scope.newNews.date.toString() == 'Invalid Date') {
            error = true;
            alert('Invalid Date');
        }

        if ($scope.newNews.title === undefined || $scope.newNews.title === '') {
            error = true;
            alert('Please enter a title');
        }

        if (document.getElementById("siofu_input").files[0] === undefined) {
            error = true;
            alert('You must select a photo');
        }
        else {
            if (document.getElementById("siofu_input").files[0].name.indexOf(' ') >= 0) {
                error = true;
                alert('I told you no spaces in the photo name')
            }
            else {
                $scope.newNews.imageURL = document.getElementById("siofu_input").files[0].name;
                uploader.submitFiles(document.getElementById("siofu_input").files) // image
            }
        }

        if (document.getElementById("siofu_input_video").files[0] !== undefined) {
            if (document.getElementById("siofu_input_video").files[0].name.indexOf(' ') >= 0) {
                error = true;
                alert('I told you no spaces in the video name')
            }
            else {
                $scope.newNews.videoURL = document.getElementById("siofu_input_video").files[0].name;
                uploader.submitFiles(document.getElementById("siofu_input_video").files) // video
            }
        }

        uploader.addEventListener("progress", function(event) {
            var percent = event.bytesLoaded / event.file.size * 100;

            $scope.percentUploaded = percent.toFixed(2);
            $scope.$apply($scope.percentUploaded)

        });


        $scope.newNews._id = (1e4 * (Date.now() + Math.random())).toString(16);
        $scope.newNews.page = $scope.pageURL;
        
        if (!error) {


            socket.emit('saveArticle', $scope.newNews);
            document.getElementById('addArticleSave').innerHTML = '<img src="images/loading.gif" />'
            $scope.showPercentUploading = true;
            
            if ($scope.newNews.videoURL === undefined) {
                setTimeout(function() {
                    closeModal("#addNews");
                    location.reload();
                }, 5000);
            }
        }



    }

    socket.on('closeNewsModal', function() {
        closeModal("#addNews");
        location.reload();
    });

    /*
     **Article page
     */

    // Close custom modal
    function closeModal(modalToClose) {
        $(modalToClose).toggleClass("reveal");
        $(".curtain").fadeOut(500).toggleClass("active");
        setTimeout(function() {
            $("body").children(".curtain").remove();
        }, 500);
    }

    // Toggle the edit article custom modal
    $scope.toggleEditNewsModal = function() {
        closeModal("#editArticle");
    }

    // Delete an article
    $scope.deleteArticle = function(article) {
        $scope.$emit('deleteNewsArticle', article._id);
        socket.emit('deleteArticle', article._id);
        $('#confirmDelete').foundation('reveal', 'close');
        $location.path('/' + article.page);
        location.reload();
    }

    // Copy the news article
    $scope.editArticleOpen = function(article) {
        $scope.editNewsArticle = angular.copy(article);
    }

    // Article is saved and sent to the database.
    $scope.editArticleSave = function() {
        var day = $scope.editNewsArticle.selectDate.substring(0, 2);
        var month = $scope.editNewsArticle.selectDate.substring(3, 5);
        var year = $scope.editNewsArticle.selectDate.substring(6, 10);
        var fullDate = month.concat('/').concat(day).concat('/').concat(year);
        $scope.editNewsArticle.date = new Date(fullDate);
        socket.emit('editArticle', $scope.editNewsArticle);
        $scope.article = angular.copy($scope.editNewsArticle);
        closeModal("#editArticle");

    }

    // Used for pagination                                                      
    $scope.changePage = function(num) {
        $scope.pageNumber = num;
    }



});