var profileController = angular.module('profileController', []);

profileController.controller('ProfileController', function($scope) {

    $scope.profile = {};
    $scope.existing = {};
    $scope.new = {};
    $scope.confirm = {};
    
    // Check if the user has access
    var checkForAccess = setInterval(function() {
        if ($scope.access !== undefined) {
            socket.emit('profileDetailsRequest')
            clearInterval(checkForAccess);
        }
    }, 200);



    socket.on('profileDetailsResult', function(user) {
        $scope.profile = user
        $scope.$apply($scope.profile)
    });

    // Function for changing the password
    $scope.updatePassword = function() {
        if ($scope.existing.password === undefined || $scope.existing.password === '') {
            $scope.changePasswordAlert = "Please enter your current password";
            $scope.showAlert();
        }
        else if ($scope.new.password === undefined || $scope.new.password === '') {
            $scope.hideAlert();
            $scope.changePasswordAlert = "Please enter your new password";
            setTimeout(function() {
                $scope.showAlert();
            }, 200);
        }
        else if ($scope.new.password !== $scope.confirm.password) {
            $scope.hideAlert();
            $scope.changePasswordAlert = "Your new passwords do not match. Please re-enter them";
            setTimeout(function() {
                $scope.showAlert();
            }, 200);
        }
        else {
            socket.emit('changePassword', $scope.existing.password, $scope.new.password); // If the user correctly enters details, new details are sent to database, and user prompted with a success msg
        }
    }


    $scope.showAlert = function() {
        $("#alert-changePassword").addClass("active");
    }
    $scope.hideAlert = function() {
        $("#alert-changePassword").removeClass("active");
    }
    
    // Success message for change password.
    socket.on('changePasswordResponse', function(text, success) {
        if (success) {
            $scope.hideAlert();
            $("#alert-changePassword").removeClass("alert").addClass("success");
            setTimeout(function() {
                $scope.showAlert();
            }, 200);
            setTimeout(function() {
                $scope.hideAlert();
            }, 4000);
        }
        else {
            $scope.showAlert();
        }
        $scope.changePasswordAlert = text;
        $scope.existingPassword = '';
        $scope.newPassword = '';
        $scope.confirmPassword = '';
        $scope.$apply()
    });



});
