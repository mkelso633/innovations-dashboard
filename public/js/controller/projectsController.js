var projectsController = angular.module('projectsController', []);

projectsController.controller('ProjectsController', function($scope, $location) {

    // Sets the default amount of projects to be displayed on one page.
    $scope.pageSize = 20;

    // Used for pagination
    $scope.changePage = function(num) {
        $scope.pageNumber = num;
    }

    // Gets the suffix of the url, to use for a page name.
    var url = $location.url();
    url = url.substring(10, url.length);
    $scope.pageName = url;


    var today = moment().format("DDMMYY");
    $scope.exportCsv = "dbt-" + angular.lowercase($scope.pageName) + "-export-" + today;

    $scope.csvData = [];


    $scope.historyRef = 'xxx'

    $scope.filterOpCoArray = [];
    $scope.filterRagArray = [];
    $scope.filterTypeArray = [];
    $scope.filterStageArray = [];
    $scope.filterStatusArray = [];
    $scope.filterCategoryArray = [];

    $scope.projectsList.forEach(function(project) {
        project.TimelineVisible = false;
    });

    // Sets pre-defined filter. If the page is not the scout page, the turn on the Active Status filter. This displays active projects only.
    $scope.stages.forEach(function(stage) {
        if (url === stage) {
            $scope.filterStageArray = [url];
        }
        if (url !== 'Scout' && url !== 'Other') {
            $scope.filterStatusArray = ['Active']
        }
        if (url === 'Other') {
            $scope.filterStageArray = ['Other'];
        }

    });


    setTimeout(function() {

        // Initialise the modal
        $(document).foundation('reveal', 'reflow');

    }, 30);

    // Initialise slider
    $(document).foundation({
        slider: {
            on_change: function() {

                // Get the value off the slider
                var value = $('#slider').attr('data-slider');

                $scope.percentageChoice = value;

                // If the choice is 0, set to null
                if ($scope.percentageChoice == 0) {
                    $scope.percentageChoice = null;
                }

                setTimeout(function() {
                    $scope.$apply($scope.percentageChoice);
                }, 0);

            }
        }
    });

    // Initialise dateFrom picker, if date has been chosen in dateTo, dateFrom cannot be after dateTo
    $('#dateFrom').datepicker({
        dateFormat: "dd/mm/yy",
        beforeShow: function(input, inst) {
            var maxDate = $('#dateTo').datepicker('getDate');
            $(this).datepicker('option', 'maxDate', maxDate);
        }
    });


    // Initialise dateTo picker, if date has been chosen in datefrom, dateTo cannot be before dateFrom
    $('#dateTo').datepicker({
        dateFormat: "dd/mm/yy",
        beforeShow: function(input, inst) {
            var minDate = $('#dateFrom').datepicker('getDate');
            $(this).datepicker('option', 'minDate', minDate);
        }
    });

  

    // Opco filter, used in conjuction with opco filter in filters.js
    $scope.selectOpCoFilter = function(opco) {
        if (_.contains($scope.filterOpCoArray, opco)) {
            // If the array contains the opco, remove
            var index = $scope.filterOpCoArray.indexOf(opco);
            $scope.filterOpCoArray.splice(index, 1);
        }
        else {
            // If it does not contain, add to the array
            $scope.filterOpCoArray.push(opco);
        }


    }

    // Status filter (Active, progressed etc)
    $scope.selectStatusFilter = function(status) {
        if (_.contains($scope.filterStatusArray, status)) {
            // If the array contains the status, remove
            var index = $scope.filterStatusArray.indexOf(status);
            $scope.filterStatusArray.splice(index, 1);
        }
        else {
            // If it does not contain, add to the array
            $scope.filterStatusArray.push(status);
        }


    }

    // Stage filter
    $scope.selectStageFilter = function(stage) {
        if (_.contains($scope.filterStageArray, stage)) {
            // If the array contains the stage, remove
            var index = $scope.filterStageArray.indexOf(stage);
            $scope.filterStageArray.splice(index, 1);
        }
        else {
            // If it does not contain, add to the array
            $scope.filterStageArray.push(stage);
        }


    }

    // Type filter
    $scope.selectTypeFilter = function(type) {
        if (_.contains($scope.filterTypeArray, type)) {
            // If the array contains the type, remove
            var index = $scope.filterTypeArray.indexOf(type);
            $scope.filterTypeArray.splice(index, 1);
        }
        else {
            // If it does not contain, add to the array
            $scope.filterTypeArray.push(type);
        }


    }

    // Rag Status filter
    $scope.selectRagFilter = function(rag) {
        if (_.contains($scope.filterRagArray, rag)) {
            // If the array contains the rag status, remove
            var index = $scope.filterRagArray.indexOf(rag);
            $scope.filterRagArray.splice(index, 1);
        }
        else {
            // If it does not contain, add to the array
            $scope.filterRagArray.push(rag);
        }


    }

    // Category filter
    $scope.selectCategoryFilter = function(name) {
        if (_.contains($scope.filterCategoryArray, name)) {
            // If the array contains the category, remove
            var index = $scope.filterCategoryArray.indexOf(name);
            $scope.filterCategoryArray.splice(index, 1);
        }
        else {
            // If it does not contain, add to the array
            $scope.filterCategoryArray.push(name);
        }
    }

    // Checks if the filters on, used for styling of buttons
    $scope.checkForFilterOn = function(opco) {

        if (_.contains($scope.filterOpCoArray, opco)) {
            return true;
        }
        if (_.contains($scope.filterRagArray, opco)) {
            return true;
        }
        if (_.contains($scope.filterStageArray, opco)) {
            return true;
        }
        if (_.contains($scope.filterStatusArray, opco)) {
            return true;
        }
        if (_.contains($scope.filterTypeArray, opco)) {
            return true;
        }
        if (_.contains($scope.filterCategoryArray, opco)) {
            return true;
        }

    }

    $scope.getDataForCsv = function(projectsList) {

        $scope.projectsList.forEach(function(obj) {

            var opcoArray = [];

            obj.opCo.forEach(function(opco) {
                opcoArray.push(opco.name)

            });

            obj.opcoArray = opcoArray.toString().replace(/,/g, "; ");;
            var selectedCat;
            $scope.categories.forEach(function(cat) {
                if (cat.name === obj.category) {
                    selectedCat = cat.title;
                }
            })

            $scope.csvData.push({
                title: obj.title,
                type: obj.type,
                category: selectedCat,
                description: obj.description,
                ragStatus: obj.ragStatus,
                opCo: obj.opcoArray,
                stage: obj.stage,
                status: obj.status,
                startDate: obj.projectStartDate,
                endDate: obj.projectEndDate,
                projectRef: obj.reference

            });

        });

    };

    $scope.createCsv = function() {
        $scope.csvArray = _.map($scope.csvData, function(o) {
            return _.pick(o, 'title', 'type', 'category', 'description', 'ragStatus', 'opCo', 'stage', 'status', 'startDate', 'endDate', 'projectRef');
        });

    }


    $scope.resetCsv = function() {
        $scope.csvData = '';
    }

    // Reset the filters and filter arrays when the clear filter button is pressed
    $scope.resetFilters = function() {
        $('#slider').foundation('slider', 'set_value', 0);
        $scope.dateFrom = undefined;
        $scope.dateTo = undefined;
        selectedDate = false;
        $scope.search = '';
        $scope.filterOpCoArray = [];
        $scope.filterRagArray = [];
        $scope.filterTypeArray = [];
        $scope.filterStatusArray = [];
        $scope.filterCategoryArray = [];
        $scope.filterStageArray = [url];
    }

    // Change project to historyProject used for timeline (past events)
    $scope.historyModal = function(project) {
        $scope.historyStage = project.stage;
        $scope.historyRef = project.reference;
    }



});