var stakeholderController = angular.module('stakeholderController', []);

stakeholderController.controller('stakeholderController', function($scope, $location) {
    $scope.compact = true; //keeo it this way compacted by default

    // Check the users access
    function checkAccess() {
        if ($scope.access !== undefined) {
            accessLoaded()
            clearInterval(checkInterval)
        }
    }
    checkAccess();

    var checkInterval = setInterval(function() {
        checkAccess();
    }, 200)

    // If the user does not have access, they are redirected
    function accessLoaded() {
        if ($scope.access <= 2) {
            $location.path('home');
        }
    }

    var currentDate = new Date();
    var currentYear = currentDate.getFullYear();
    $scope.historyRef = 'xxx'
    $scope.historyStage = 'xxx'
    $scope.previousYear = currentYear - 1;
    $scope.currentYear = currentYear;
    $scope.nextYear = currentYear + 1;
    $scope.stakeholderList = [];
    $scope.filteredStakeholderList = [];

    $scope.filterOpCoArray = [];
    $scope.filterCategoryArray = [];

    // Initialising modal and datepicker
    setTimeout(function() {
        $(document).foundation('reveal', 'reflow');

        $('#dateFromStakeholder').datepicker({
            dateFormat: "dd/mm/yy",
        });


    }, 30);

    setTimeout(tjhAccordion, 10);

    $scope.selectOpCoFilter = function(opco) {
        if (_.contains($scope.filterOpCoArray, opco)) {
            //contains
            var index = $scope.filterOpCoArray.indexOf(opco);
            $scope.filterOpCoArray.splice(index, 1);
        }
        else {
            //does not contain
            $scope.filterOpCoArray.push(opco);
        }
    }

    $scope.selectCategoryFilter = function(name) {
        if (_.contains($scope.filterCategoryArray, name)) {
            //contains
            var index = $scope.filterCategoryArray.indexOf(name);
            $scope.filterCategoryArray.splice(index, 1);
        }
        else {
            //does not contain
            $scope.filterCategoryArray.push(name);
        }
    }

    $scope.checkForFilterOn = function(opco) {

        if (_.contains($scope.filterOpCoArray, opco)) {
            return true;
        }

        if (_.contains($scope.filterCategoryArray, opco)) {
            return true;
        }
    }

    // Reset the filters, this is used for clear filters button
    $scope.resetFilters = function() {
        $scope.dateFrom = undefined;
        selectedDate = false;
        $scope.search = '';
        $scope.filterOpCoArray = [];
    }

    // Change project to historyProject used for timeline (past events)
    $scope.historyModal = function(project) {
        $scope.historyProject = project
    }

    // Reduces all year variables by one (year), resets the stakeholder list to be empty, and then reloads the stakeholder data to update for current year
    $scope.viewPrevYear = function() {
        currentYear--;
        $scope.previousYear = --$scope.previousYear;
        --$scope.currentYear;
        $scope.nextYear = --$scope.nextYear;
        $scope.filteredStakeholderList = [];
        stakeholderLoad()
    }

    // Increases all year variables by one (year), resets the stakeholder list to be empty, and then reloads the stakeholder data to update for current year
    $scope.viewNextYear = function() {
        currentYear++;
        $scope.previousYear = ++$scope.previousYear;
        ++$scope.currentYear;
        $scope.nextYear = ++$scope.nextYear;
        $scope.filteredStakeholderList = [];
        stakeholderLoad()

    }

    // Load the stakeholderList, makes changes and creates new keys. Needed to display timelines on stakeholder page.
    function stakeholderLoad() {

        /* Blank Array */
        var analyticsArray = [];

        /* Sorting the stakeholderList by project start date */
        var sortedStakeholderArray = _.sortBy($scope.stakeholderList, 'projectStartDate');

        /* Group the projects by project reference. This group all projects with timeline (Scout -> Live) */
        var groupProjects = _.map(_.groupBy(sortedStakeholderArray, "reference"), function(group, reference) { // group by reference
            return _.reduce(group, function(item, elem) {

                item.projectLength += elem.projectLength;

                // Push the stage of each stage to an array
                item.stage.push(elem.stage)

                // Push the title of each stage to an array 
                item.title.push(elem.title)

                // Push the category of each stage to an array 
                item.category.push(elem.category)

                // Push the start date of each stage to an array
                item.date.push(elem.projectStartDate)

                // Push the end date of each stage to an array 
                item.date.push(elem.projectEndDate)

                // Set variable to the first date in the array 
                var newProjectStartDate = item.date[0];

                // Convert to a moment date format 
                item.newProjectStartDate = moment(newProjectStartDate);

                // Set the variable to the last day in the array
                var newProjectEndDate = item.date[item.date.length - 1];

                // Convert to a moment date format
                item.newProjectEndDate = moment(newProjectEndDate);

                item.opCo = elem.opCo;

                // Store opco of each stage (whether its singular or multi)
                item.opCoArray.push(elem.opCo);

                // Set the variable to the last title in the array
                item.projectTitle = item.title[item.title.length - 1];

                // Set the variable to the last category in the array
                item.projectCategory = item.category[item.category.length - 1];

                // Push the item obj to a new array
                analyticsArray.push(item)


                return item;

            }, {
                // Item obj
                reference: reference,
                projectLength: 0,
                projectDateLength: 0,
                opCo: '',
                opCoArray: [],
                projectTitle: '',
                projectCategory: '',
                stage: [],
                date: [],
                title: [],
                category: [],
                newProjectStartDate: undefined,
                newProjectEndDate: undefined,
                lastProjectTitle: '',
            });

        });

        //Strips duplicates
        var analyticsArrayNew = _.filter(analyticsArray, function(element, index) {
            for (index += 1; index < analyticsArray.length; index += 1) {
                if (_.isEqual(element, analyticsArray[index])) {
                    return false;
                }
            }
            return true;

        });

        // For loop to remove opcos out of array of objects to an array of strings
        analyticsArrayNew.forEach(function(project) {

            var opcoArray = [];

            project.opCoArray.forEach(function(opco) {
                opco.forEach(function(item) {
                    opcoArray.push(item.name)
                });

            });

            // Set key to the new string array
            project.opcoArray = opcoArray;

        });


        // loops through each "project" - really a project is an individual SABLO
        $scope.stakeholderList.forEach(function(project) {

            var stakeholderStart;
            var stakeholderEnd;
            var stageArray = [];
            //var excludeScout = false;
            var currentProjectTitle;
            var currentCategory;
            var stakeHolderEndTest;
            var opcoArray = [];

            // setting each project to have the last project title
            analyticsArrayNew.forEach(function(item) {
                if (project.reference == item.reference) {

                    stakeholderStart = item.newProjectStartDate;
                    stakeholderEnd = item.newProjectEndDate;
                    stageArray = item.stage;

                    // get project title of active stage
                    currentProjectTitle = item.projectTitle;
                    currentCategory = item.projectCategory
                    opcoArray = item.opcoArray;

                }
            });
            

            // Excludes projects which have one scout to declutter the view (there could be hundreds of scouts that are killed). If the scout is a catapult, then it's included
            //turned off by request of dan. Now wants all scouts to show even if killed. But will only display them if a category is applied
            /*if (stageArray.length == 1 && _.contains(stageArray, "Scout") && project.type !== "Catapult") {
                excludeScout = true;
            }
            */
            // CREATE THE BINDS FOR THE BUTTONS ON THE TIMELINE
            project.currentProjectTitle = currentProjectTitle;
            project.currentCategory = currentCategory;
            project.opcoArray = opcoArray;

            var stakeholderStartYear = stakeholderStart.format("YYYY");
            var stakeholderEndYear = stakeholderEnd.format("YYYY");

            var projectStartYear = moment(project.projectStartDate)._d;
            projectStartYear = projectStartYear.getFullYear();

            var projectEndYear = moment(project.projectEndDate)._d;
            projectEndYear = projectEndYear.getFullYear();

            // if the project starts and ends in the current year
            if (projectStartYear == currentYear && projectEndYear == currentYear) {
                var startDayInYear = moment(project.projectStartDate).format("DDD");
                project.startPercentYear = (startDayInYear - 1) / 365 * 100;

                var endDayInYear = moment(project.projectEndDate).format("DDD");
                project.endPercentYear = 100 - (endDayInYear / 365 * 100);
            }
            // else if project begins before and ends in the current year
            else if (projectStartYear < currentYear && projectEndYear == currentYear) {
                project.startPercentYear = 0;
                //console.log(stakeholderEnd)
                var endDayInYear = moment(project.projectEndDate).format("DDD");
                project.endPercentYear = 100 - (endDayInYear / 365 * 100);

            }
            // else if project begins in and ends after the current year
            else if (projectStartYear == currentYear && projectEndYear > currentYear) {
                var startDayInYear = moment(project.projectStartDate).format("DDD");
                project.startPercentYear = (startDayInYear - 1) / 365 * 100;
                project.endPercentYear = 0;
            }
            // else if the project begins before and ends after the current year
            else if (projectStartYear < currentYear && projectEndYear > currentYear) {
                project.startPercentYear = 0;
                project.endPercentYear = 0;

            }
            // CREATE THE BINDS FOR THE LINES ON THE TIMELINE
            // if project starts and ends in the current year
            // shit
            if (stakeholderStartYear == currentYear && stakeholderEndYear == currentYear) {
                project.stakeholderStart = stakeholderStart.format("DDD") / 365 * 100;
                project.stakeholderEnd = 100 - stakeholderEnd.format("DDD") / 365 * 100;
            }
            // else if project starts before and ends in current year
            else if (stakeholderStartYear < currentYear && stakeholderEndYear == currentYear) {
                project.stakeholderStart = 0;
                project.stakeholderEnd = 100 - stakeholderEnd.format("DDD") / 365 * 100;
            }
            // else if project starts in and ends after current year
            else if (stakeholderStartYear == currentYear && stakeholderEndYear > currentYear) {
                project.stakeholderStart = stakeholderStart.format("DDD") / 365 * 100;
                project.stakeholderEnd = 0;
            }
            // else if project starts before and ends after current year
            else if (stakeholderStartYear < currentYear && stakeholderEndYear > currentYear) {
                project.stakeholderStart = 0;
                project.stakeholderEnd = 0;
            }
            // otherwise set left pos to be 100% and left to 0, to make the width of the line 0
            else {
                project.stakeholderStart = 100;
                project.stakeholderEnd = 0;
            }

            //if (excludeScout == false) {
            $scope.filteredStakeholderList.push(project) // push to filtered array

            //}
            


        });


        setTimeout(function() {
            $scope.$apply($scope.stakeholderList);
        })



    }; // end stakeholderLoad

    var checkForProjectsData2 = setInterval(function() {
        if ($scope.projectsList.length !== 0) {
            angular.copy($scope.projectsList, $scope.stakeholderList);
            stakeholderLoad();
            clearInterval(checkForProjectsData2);
        }
    }, 10);


});