 /* Filters used within dashboard */
 var filters = angular.module('Dashboard.filters', []);

 var selectedDate = false;

 filters.filter('removeSpaces', [function() {
     return function(string) {
         if (!angular.isString(string)) {
             return string;
         }
         return string.replace(/[\s]/g, '');
     };
 }])

 /* Used to get first paragraph of news item description for display on news pages */
 filters.filter('firstParagraph', [function() {
     return function(string) {
         if (!angular.isString(string)) {
             return string;
         }
         var paraBreak = string.indexOf("\n");

         if (paraBreak > 0) {
             string = string.substr(0, paraBreak);
         }
         return string;

     };
 }])

 /* Used with the datepicker to display / remove projects from view */
 filters.filter("dateFilter", function() {

     return function(array, from, to) {
         selectedDate = false;

         var dateFrom = from;
         var dateTo = to;

         if (dateFrom !== undefined || dateTo !== undefined) {
             selectedDate = true;
         }

         var dateFilterArray = [];

         if (selectedDate == true) {

             if (dateFrom !== undefined) {

                 var day = dateFrom.substring(0, 2);
                 var month = dateFrom.substring(3, 5);
                 var year = dateFrom.substring(6, 10);
                 var fullFromDate = month.concat('/').concat(day).concat('/').concat(year);

                 var dateFromConverted = new Date(fullFromDate); // date from
             }

             if (dateTo !== undefined) {

                 var day = dateTo.substring(0, 2);
                 var month = dateTo.substring(3, 5);
                 var year = dateTo.substring(6, 10);
                 var fullToDate = month.concat('/').concat(day).concat('/').concat(year);
                 var dateToConverted = new Date(fullToDate); // date to 

             }

             array.forEach(function(project) {

                 var startDateConverted = new Date(project.projectStartDate); // component start date

                 var endDateConverted = new Date(project.projectEndDate); // component end date

                 if (dateFrom !== undefined && dateTo !== undefined) {
                     if (startDateConverted >= dateFromConverted && startDateConverted <= dateToConverted || endDateConverted >= dateFromConverted && endDateConverted <= dateToConverted) {
                         dateFilterArray.push(project);
                     }
                 }

                 else if (dateFrom !== undefined) {
                     if (startDateConverted >= dateFromConverted) {
                         dateFilterArray.push(project);
                     }
                 }

                 else if (dateTo !== undefined) {
                     if (endDateConverted <= dateToConverted) {
                         dateFilterArray.push(project);
                     }
                 }

             });

             return dateFilterArray;
         }
         else {
             return array;
         }

     };
 });


 /* Date filter used on stakeholder */
 filters.filter("stakeholderDateFilter", function() {

     return function(array, from) {

         selectedDate = false;

         var dateFrom = from;

         if (dateFrom !== undefined) {
             selectedDate = true;
         }

         var dateFilterArray = [];

         if (selectedDate == true) {

             if (dateFrom !== undefined) {

                 var fullFromDate = moment(dateFrom, "DD-MM-YYYY");
             }

             array.forEach(function(item) {

                 var projectStart = moment(item.projectStartDate);

                 if (dateFrom !== undefined) {
                     if (projectStart >= fullFromDate) {
                         dateFilterArray.push(item);

                     }
                 }


             });

             return dateFilterArray;
         }
         else {
             return array;
         }

     };
 });



 /* OpCo filter on  stakeholder */
 filters.filter('selectedFilter', function() {
     return function(projectsList, filterArray, key) {

         if (filterArray.length !== 0) {
             var returnProjectsList = [];

             if (key === 'opCo') {
                 projectsList.forEach(function(project) {
                     filterArray.forEach(function(filterItem) {
                         if (_.contains(project.opcoArray, filterItem)) {
                                 if(!containsObject(project,returnProjectsList)) {
                             returnProjectsList.push(project);
                              }
                         };
                     });

                 });

             }
             else {
                 projectsList.forEach(function(project) {
                     filterArray.forEach(function(filterItem) {
                         if (filterItem === 'Other') {
                             if ('Other1' === project[key] || 'Other2' === project[key] || 'Other3' === project[key]) {
                                 returnProjectsList.push(project);
                             }
                         }
                         else if (filterItem === project[key]) {
                             returnProjectsList.push(project);
                         }
                     });
                 });
             }

             return returnProjectsList;
         }
         else {
             return projectsList;
         }
     };
 });

 /* Used to sort the order of the buttons on the timeline */
 filters.filter('buttonSort', function() {
     return function(array, reverse) {

         var scoutArray = [];
         var other1Array = [];
         var alphaArray = [];
         var other2Array = [];
         var betaArray = [];
         var other3Array = [];
         var liveArray = [];
         var blankArray = [];

         array.forEach(function(project) {
             if (project.stage === 'Scout') {
                 scoutArray.push(project);
             }
             else if (project.stage === 'Other1') {
                 other1Array.push(project);
             }
             else if (project.stage === 'Alpha') {
                 alphaArray.push(project);
             }
             else if (project.stage === 'Other2') {
                 other2Array.push(project);
             }
             else if (project.stage === 'Beta') {
                 betaArray.push(project);
             }
             else if (project.stage === 'Other3') {
                 other3Array.push(project);
             }
             else if (project.stage === 'Live') {
                 liveArray.push(project);
             }
             else if (project.stage === '') {
                 blankArray.push(project);
             }
         });

         if (!reverse) {
             var returnArray = scoutArray.concat(other1Array).concat(alphaArray).concat(other2Array).concat(betaArray).concat(other3Array).concat(liveArray)
         }
         else {
             var returnArray = blankArray.concat(liveArray).concat(other3Array).concat(betaArray).concat(other2Array).concat(alphaArray).concat(other1Array).concat(scoutArray)
         }


         return returnArray;
     }

 });

 filters.filter('slice', function() {
     return function(arr, start, end) {
         return (arr || []).slice(start, end);
     };
 });

 filters.filter("sanitize", ['$sce', function($sce) {
     return function(htmlCode) {
         return $sce.trustAsHtml(htmlCode);
     }
 }]);

 /* Removes irrelevant data from event carousel on the homepage */
 filters.filter('eventCarouselFilter', function() {
     return function(eventArray) {

         var eventFilterArray = [];

         var todaysDate = new Date();

         todaysDate.setHours(0, 0, 0, 0);

         eventArray.forEach(function(event) {

             var eventStartDate = new Date(event.start);

             var eventEndDate = new Date(event.end);

             if (event.allDay == true) {

                 eventEndDate = new Date(event.displayEndDate)

             }

             eventStartDate.setHours(0, 0, 0, 0)

             eventEndDate.setHours(0, 0, 0, 0)

             if (event.className !== "cat-leave") {
                 if (eventStartDate >= todaysDate || eventStartDate < todaysDate && eventEndDate >= todaysDate) {

                     eventFilterArray.push(event)

                 }
             }

         });

         return eventFilterArray;

     }
 });


 /* Used on the stakeholder page, to remove timelines which arent in the displayed year */
 filters.filter('removeHistorical', function() {
     
     return function(data, currentYear) {

         var stakeholderArray = [];

         data.forEach(function(stakeholder) {

             var startDate = stakeholder.projectStartDate;
             var startYear = stakeholder.projectStartDate.substring(0, 4);

             var endDate = stakeholder.projectEndDate;
             var endYear = stakeholder.projectEndDate.substring(0, 4);

             if (startYear <= currentYear && endYear >= currentYear) {
                 stakeholderArray.push(stakeholder);
             }

         });

         return stakeholderArray;

     }
 });

 /* Used on the stakeholder page, to remove timelines which arent in the displayed year */
 filters.filter('removeProjectsByYear', function() {
     return function(data, currentYear, compact) {
         var returnArray = [];

         if (compact == true) {

             data.forEach(function(stakeholder) {

                 if (stakeholder.stakeholderStart !== 100) {
                     returnArray.push(stakeholder)
                 }

             });
             return returnArray;
         }
         else {
             return data;
         }
     }
 });

 function containsObject(obj, list) {
     var i;
     for (i = 0; i < list.length; i++) {
         if (list[i] === obj) {
             return true;
         }
     }

     return false;
 }


 /* OpCo filter for SABL pages */
 filters.filter('selectedOpCoFilter', function() {
     return function(projectsList, filterArray, key) {
         if (filterArray.length !== 0) {
             var returnProjectsList = [];
             if (key === 'opCo') {
                 projectsList.forEach(function(project) {
                     project.opCo.forEach(function(opco) {
                         filterArray.forEach(function(filterItem) {
                             if (filterItem === opco.name) {
                                 if (!containsObject(project, returnProjectsList)) {
                                     returnProjectsList.push(project);
                                 }
                             }
                         });
                     });
                 });
             }
             return returnProjectsList;
         }
         else {
             return projectsList;
         }
     };
 });