function removeFunction(myObjects, prop, valu) {
    return myObjects.filter(function(val) {
        return val[prop] !== valu;
    });
}

function findIndexByKeyValue(obj, key, value) {
    for (var i = 0; i < obj.length; i++) {
        if (obj[i][key] == value) {
            return i;
        }
    }
    return null;
}

function findAllInArray(obj, key, value) {
    var returnArray = []
    for (var i = 0; i < obj.length; i++) {
        if (obj[i][key] == value) {
            returnArray.push(obj[i]);
        }
    }
    if (!returnArray) {
        return obj
    }
    return returnArray;
}

var idleTime = 0;

// auto refresh
$(document).ready(function() {
    //Increment the idle time counter every minute.
    var idleInterval = setInterval(timerIncrement, 60000); // 1 minute

    //Zero the idle timer on mouse movement.
    $(this).mousemove(function(e) {
        idleTime = 0;
    });
    $(this).keypress(function(e) {
        idleTime = 0;
    });
});

function timerIncrement() {
    idleTime++;
    if (idleTime > 29) { // 29 = 30 mins
        window.location.reload();
    }
}


/*document.addEventListener("paste", function(e) {
    // cancel paste
    e.preventDefault();

    // get text representation of clipboard
    var text = e.clipboardData.getData("text/plain");

    // insert text manually
    document.execCommand("insertHTML", false, text);
});*/


function httpData(data) {

    if (data === undefined || data == '' || data == ' ') {
        return;
    }
    else {
        if (data.substring(0, 4) !== 'http')
            data = 'http://'.concat(data);
        return data;

    }
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}


function tjhAccordion() {

    $(".tjh-accordion").each(function(index) {
        var accordionAutoOpen = $(this).attr("data-auto-open");
        if (accordionAutoOpen == 'true') {
            $(this).children("li").each(function(index) {
                $(this).addClass("active");
            });
        }

    });

    $('.tjh-accordion-title').click(function(e) {




        // prevent the link from taking you anywhere
        e.preventDefault();

        var $accordionSection = $(this).parent();
        var $accordion = $accordionSection.parent();
        var $accordionContent = $accordionSection.children(".tjh-accordion-container");

        // only allow one section open at a time?
        var autoCollapse = $accordion.attr("data-auto-collapse");

        // if accordionSection is open ("active") then close it
        if ($accordionSection.hasClass("active")) {
            $accordionSection.removeClass("active");
            $accordionContent.slideUp();
        }
        // else if accordionSection is not oppen...
        else {
            // close the currently open section
            if (autoCollapse === "true") {
                $toClose = $accordion.children("li.active");
                $toClose.removeClass("active");
                $toClose.children(".tjh-accordion-container").slideUp();
            }
        
            // open the selected section
            $accordionSection.addClass("active");
            $accordionContent.slideDown();

        }
    });
}

$(document).ready(function() {
    $(document).on('click', '#logo', function() {
        if ($('.curtain').length) {
            $("body").children(".curtain").remove();
        }
    });

    $(document).on('click', '.filter-btn', function(e) {
        toggleFilters(e);
    });
    $(document).on('click', '#hamburger, #close-nav', function(e) {
        toggleNavigation();
    });

    $(document).on('click', '#sidebar-menu a', function(e) {
        toggleNavigation();
        $(".curtain").fadeOut(500);
        setTimeout(function() {
            $("body").children(".curtain").remove();
        }, 500);
    });

    function toggleNavigation() {
        $("#main-container").toggleClass("active");
        $("#sidebar-menu").toggleClass("active");
        $("#sidebar-menu-container").scrollTop(0);
    }

    function toggleFilters(e) {
        e.preventDefault();
        // if filters open
        if ($(".filters").hasClass("active")) {
            $(".filters").toggleClass("active");
            $(".curtain").fadeOut(500);
            setTimeout(function() {
                $("body").children(".curtain").remove();
            }, 500);
        }
        // else open filters
        else {
            $("body").append('<div class="curtain"></div><!--curtain-->');
            $(".curtain").fadeIn(500);
            $(".filters").scrollTop(0).toggleClass("active");
        }
    }

    $(document).on('click', '.curtain', function(e) {
        if ($(".modal").hasClass("reveal")) {
            $(".modal").removeClass("reveal")
        }
        if ($(".filters").hasClass("active")) {
            $(".filters").removeClass("active")
        }
        $(".curtain").fadeOut(500);
        setTimeout(function() {
            $("body").children(".curtain").remove();
        }, 500);
    });

    $(document).on('click', '.toggle-modal', function(e) {

        e.preventDefault();
        $targetModal = $(this).attr("data-toggle-modal");
        $("body").append('<div class="curtain curtain-modal"></div><!--curtain-->');
        $(".curtain").fadeIn(500);
        $("#" + $targetModal).toggleClass("reveal");


    });
    $(document).on('click', '.close-modal', function(e) {
        e.preventDefault();
        closeModal();
    });

});

function closeModal() {
    $(".curtain").fadeOut(500);
    $(".modal.reveal").toggleClass("reveal");
    setTimeout(function() {
        $("body").children(".curtain").remove();
    }, 500);
}

function closeTimelineModal(){
    $('#historyModal').foundation('reveal', 'close');
}

// always make sure it is https
if (window.location.protocol != "https:") {
    window.location.href = "https:" + window.location.href.substring(window.location.protocol.length);
}
