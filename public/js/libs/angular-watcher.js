// Angular watcher
// G.R Nov 2014
// Written in native javascript as we cannot bind in the Angular $digest as that's what we're interrogating
// and I don't want jquery as a dependency
var angularWatcher = function($scope) {
    
	var angularWatches = {};
    var element = null;
    var showDetails = false;
    var traversing = false;
    var bindCount = 0;
    var bindings = [];
    var windowHeight = 250;

    var addWatch = function(w) {
        angularWatches[w] = w;      
    };
    
    var toggleDetails = function() { 
    	this.showDetails = !this.showDetails; 
    	
    };
    
    
    var updateWatches = function($scope){
		
		target = $scope;
		
		if(this.element) {
			
			adjustedHeight = this.showDetails ? this.windowHeight : 100;
			this.element.style.top = window.pageYOffset + (window.innerHeight - adjustedHeight) + "px";

			dirty = false;
			current = target;

			var dirtyCount = 0;
			var bindCount = 0;
			this.bindings = [];
			var indent = "";
			
			traverseScopesLoop:
			do { // "traverse the scopes" loop

				if ((watchers = current.$$watchers)) {
					
					// process our watches
					length = watchers.length;

					  while (length--) {
						try {
						  watch = watchers[length];
						  
						  // Most common watches are on primitives, in which case we can short
						  // circuit it with === operator, only when === fails do we use .equals
						  if (watch) {
							if(watch.last) {
								
								var objectDetails = "";
								var type = typeof watch.last
								if(type == "object") {
									for(var props in watch.last){
										objectDetails += props + ",";
									}
								}
								if(type == "string")
								{
									var binding = watch.exp.exp;
									if(watch.last.indexOf("html") != -1)
										type = "ng-include";
									if(watch.last.indexOf("http") != -1)
										type = "url";
									if(watch.last.indexOf("#/") != -1)	
										type = "routing";
									objectDetails = "View:" + binding;
								}
								this.bindings.push({type:type,value:watch.last,details:objectDetails});
								bindCount++;
							}
						  }
						} catch (e) {
						  console.log(e);
						}
					  }

				}

				// Insanity Warning: scope depth-first traversal
				if (!(next = (current.$$childHead ||
					(current !== target && current.$$nextSibling)))) {
				  while(current !== target && !(next = current.$$nextSibling)) {
					current = current.$parent;
					//indent += "parent";
					//current = null;
					//break;
				  }
				}
				} while ((current = next));

	    		var html = "<table border=\"1\"><tr><td colspan=\"3\">Angular binding watcher    v1.1</td></tr>";
				html += "<tr><td colspan=\"2\"><b>Angular watches : " + bindCount + "</b></td>";
				html += "<td><input onclick=\"javascript:angularWatcher.toggleDetails();\" type=\"button\" value=\"Details\"></input></tr>";
				
				if(this.showDetails){
					html += "<tr><th width=\"15%\">Type</th><th width=\"15%\">Value</th><th width=\"70%\">Details</th></tr>";
					if(bindCount < 2000) {
						html += "<tr>";
						for(var w in this.bindings) {
							html += "<td><b>" + this.bindings[w].type + "</b></td><td>" + this.bindings[w].value + "</td><td style=\"max-width:500px\">";
							if(this.bindings[w].details){
								html += " " + this.bindings[w].details;
							}
							html += "</td></tr>";
						}
						html += "</table>";
					}
				}
				
				this.element.innerHTML = html;		
				this.traversing = false;	
				
				if(bindCount > 10000)
					this.element.style.backgroundColor = "#ffaaaa";
				if(bindCount > 1000 && bindCount < 10000)
					this.element.style.backgroundColor = "#ffa800";
				if(bindCount < 1000)
					this.element.style.backgroundColor = "#aaffaa";
		}        
    };
    
    return {
        
        update:updateWatches,
        element:element,
		windowHeight:windowHeight,
		showDetails:showDetails,
		toggleDetails:toggleDetails
    }

}();

document.addEventListener("DOMContentLoaded", function(event) { 
	var el = document.createElement('div');
	document.body.appendChild(el);
	angularWatcher.element = el;
	el.style.position = "absolute";
	el.style.left = "0px";
	el.style.top = window.innerHeight - angularWatcher.windowHeight + "px";
	el.style.padding = "10px";
	el.style.overflow = "scroll";
	el.style.height = "200px";
	el.style.border = "1px SOLID #a0a0a0";
	el.style.width = window.innerWidth + "px";
	el.style.backgroundColor = "#ffffff";
	el.style.color = "#000000";
	el.style.opacity = "0.8";
	el.style.zIndex = "999";
	el.style.textAlign = "left";
    el.border = "2px";
	var doc = document.documentElement;
	var top = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);

});
