var express = require('express');
var app = express()
var path = require('path');
var router = express.Router();
var https = require("https");
var fs = require("fs");
var xml2js = require('xml2js');
var parser = new xml2js.Parser();
var config = require('../config/config');

var saml = require('passport-saml');

var isAuthenticated = function(req, res, next) {
		// if user is authenticated in the session, call the next() to call the next request handler 
		// Passport adds this method to request object. A middleware is allowed to add properties to
		// request and response objects
		if (req.isAuthenticated())
			return next();
		// if the user is not authenticated then redirect him to the login page
		res.redirect('/login');
	}
	// Nodejs encryption with CTR
var crypto = require('crypto'),
	algorithm = 'aes-256-ctr',
	password = 'VM9XS9DKTJRuwwebqXIq';

function encrypt(text) {
	var cipher = crypto.createCipher(algorithm, password)
	var crypted = cipher.update(text, 'utf8', 'hex')
	crypted += cipher.final('hex');
	return crypted;
}

function decrypt(text) {
	var decipher = crypto.createDecipher(algorithm, password)
	var dec = decipher.update(text, 'hex', 'utf8')
	dec += decipher.final('utf8');
	return dec;
}




module.exports = function(passport, db) {

	router.get('/baMetadata', function(req, res) {
		var cert = fs.readFileSync('./config/myCert.pem', 'utf-8');

		var baMetadataStrategy = new saml.Strategy(passport._strategies.baSAML._saml.options,
			function() {});
		res.type('application/xml');
		res.status(200).send(baMetadataStrategy.generateServiceProviderMetadata(cert));

	});

	router.get('/ibMetadata', function(req, res) {
		var cert = fs.readFileSync('./config/myCert.pem', 'utf-8');

		var ibMetadataStrategy = new saml.Strategy(passport._strategies.ibSAML._saml.options,
			function() {});
		res.type('application/xml');
		res.status(200).send(ibMetadataStrategy.generateServiceProviderMetadata(cert));

	});

	/* GET dashboard. */
	router.get('/', isAuthenticated, function(req, res) {
		var accessLevel = req.user.Access
		if (accessLevel >= 2) {
			res.cookie('connectionid', encrypt(req.user._id));
			res.sendFile(path.join(__dirname, '../private/index.html'));
		}
		else {
			res.render('localLogin', {
				message: 'Please verify your e-mail address'
			});
		}
	});

	router.get('/admin', isAuthenticated, function(req, res) {
		var accessLevel = req.user.Access;
		if (accessLevel === 5) {
			res.sendFile(path.join(__dirname, '../private/admin.html'));
		}
		else {
			res.render('error', {
				error: {
					status: 'You do not have access to this page'
				}
			});
		}


	});
	router.get('/admin.js', function(req, res) {
		var accessLevel = req.user.Access;
		if (accessLevel === 5) {
			res.sendFile(path.join(__dirname, '../private/admin.js'));
		}
	});

	router.get("/baLogin",
		passport.authenticate('baSAML', {
			successRedirect: "/",
			failureRedirect: "/login",
		})
	);
	router.post('/balogin/callback',
		passport.authenticate('baSAML', {
			failureRedirect: '/',
			failureFlash: true
		}),
		function(req, res) {
			res.redirect('/');
		}
	);


	/* GET login page. */
	router.get('/localLogin', function(req, res) {
		// Display the Login page with any flash message, if any
		res.render('localLogin', {
			message: req.flash('message')
		});
	});

	/* Handle Login POST */
	router.post('/localLogin', passport.authenticate('localLogin', {
		successRedirect: '/',
		failureRedirect: '/localLogin',
		failureFlash: true
	}));

	/* GET Registration Page */
	router.get('/signup', function(req, res) {
		res.render('register', {
			message: req.flash('message')
		});
	});

	/* Handle Registration POST */
	router.post('/signup', passport.authenticate('localSignup', {
		successRedirect: '/signup',
		failureRedirect: '/signup',
		failureFlash: true
	}));

	/* GET verify user from e-mail link */
	router.get('/verify/:id', function(req, res) {
		console.log('user ' + req.params.id);
		var id = decrypt(req.params.id)
		db.users.update({
			_id: id
		}, {
			$set: {
				Access: 2
			}
		}, function(err) {
			if (err) {
				console.log('verify error: ' + err);
			}
			else {
				res.redirect('/');
			}

		});
	});

	/* GET forgot page */
	router.get('/forgot', function(req, res) {
		res.render('forgot', {
			message: req.flash('message')
		});
	});

	/* Handle forgot POST*/
	router.post('/forgot', function(req, res) {

		var forgotMyPass = require(path.join(__dirname, '../passport/forgot.js'));
		forgotMyPass(db, req, res);


	});


	/* Handle Logout */
	router.get('/signout', function(req, res) {

		try {
			var loginMethod = req.user.loginMethod;
		}
		catch (e) {

		}
		req.logout();
		res.clearCookie('connectionid');

		if (loginMethod === 'bSafe') {
			res.redirect(config.passport.baSAML.logoutUrl);
		}
		else {

			res.redirect('/');
		}


	});

	/* Catch any wrong urls*/
	router.get('/login', function(req, res) {
		res.render('login');
	});

	/* Catch any wrong urls*/
	router.get('/*', function(req, res) {
		res.render('login', {
			message: 'Sorry, there has been a problem. Please log back in'
		});
	});

	return router;
}
