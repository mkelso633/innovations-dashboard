var bCrypt = require('bcrypt-nodejs');
var _ = require('underscore');
var express = require('express');
var app = express();
//var async = require('async');

// SocketIO File Upload
var SocketIOFileUpload = require('socketio-file-upload');
app.use(SocketIOFileUpload.router);

// Nodejs encryption with CTR 
var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'VM9XS9DKTJRuwwebqXIq';

function decrypt(text) {
    var decipher = crypto.createDecipher(algorithm, password);
    var dec = decipher.update(text, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return dec;
}

function getCookie(cookies, cname) {
    var name = cname + "=";
    var ca = cookies.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}


module.exports = function(db, io) {

    io.on('connection', function(socket) {

        var localUser = {
            Access: 1
        };

        socket.on('disconnect', function() {

        });

        try {
            var userID = getCookie(socket.handshake.headers.cookie, 'connectionid');
            userID = decrypt(userID);
            console.log('cookieID ' + userID)
            db.users.find({
                _id: userID
            }, function(err, data) {
                if (err || !data.length) {
                    console.log('[' + new Date() + '] Could not find user Error: ' + err);
                    socket.emit('loginError');
                }
                else {
                    // found user, 
                    var user = data[0];

                    //check to see if they are a new user from bsafe and need more details about them
                    if (user.NewUser) {
                        socket.emit('NewUser');
                    }

                    var fullName = user.firstName + ' ' + user.lastName;
                    console.log('[' + new Date() + '] Login!: Username: "' + user.username + '", name: "' + fullName + '"');
                    localUser = user;
                    socket.emit('foundUser', user.Access, fullName, user.loginMethod);
                    socket.username = user.username,
                        socket.fullName = fullName;
                    if (user.Access >= 2) {
                        setTimeout(function() {
                            loadHomepageNumbers();
                            loadProjects(user.Access);
                            loadEvents(user.Access);
                            loadAllNews();
                            setupSIOFU();

                        }, 10);
                    }

                }
            });
        }
        catch (err) {
            socket.emit('loginError');
        }


        socket.on('updateNewUserDetails', function(fName, lName, opco) {
            db.users.update({
                username: localUser.username
            }, {
                $set: {
                    firstName: fName,
                    lastName: lName,
                    OpCo: opco,
                    NewUser: false
                }
            });
        });



        var loadProjects = function(accessLevel) {
            if (accessLevel >= 3) {
                db.projects.find({}, function(err, data) {
                    if (err || !data.length) {
                        console.log('[' + new Date() + '] Could not find projects Error: ' + err)
                    }
                    else {
                        socket.emit('projectsListLoad', data)
                    }
                });
            }

            /* else if (accessLevel === 4) {
                 db.projects.find({}, {
                     oneNote: 0
                 }, function(err, data) {
                     if (err || !data.length) {
                         console.log('[' + new Date() + '] Could not find projects Error: ' + err)
                     }
                     else {
                         socket.emit('projectsListLoad', data)
                     }
                 });
             }*/
            else {
                db.projects.find({
                    stage: {
                        $nin: ["Other1", "Other2", "Other3"]
                    }
                }, {
                    oneNote: 0
                }, function(err, data) {
                    if (err || !data.length) {
                        console.log('[' + new Date() + '] Could not find projects Error: ' + err)
                    }
                    else {

                        socket.emit('projectsListLoad', data)
                    }
                });
            }

        }

        function loadEvents(accessLevel) {
            if (accessLevel === 5) {
                db.events.find({}, function(err, data) {
                    if (err || !data.length) {
                        console.log('[' + new Date() + '] Could not find events Error: ' + err);
                    }
                    else {
                        socket.emit('eventsListLoad', data);
                    }
                });
            }
            else if (accessLevel >= 3) {
                db.events.find({
                    className: {
                        $nin: ["cat-leave", "cat-dbt", "cat-deadline"]
                    }
                }, function(err, data) {
                    if (err || !data.length) {
                        console.log('[' + new Date() + '] Could not find events Error: ' + err);
                    }
                    else {
                        socket.emit('eventsListLoad', data);
                    }
                });
            }
            else {
                db.events.find({
                    className: {
                        $nin: ["cat-leave", "cat-dbt", "cat-deadline", "cat-stakeholder"]
                    }
                }, function(err, data) {
                    if (err || !data.length) {
                        console.log('[' + new Date() + '] Could not find events Error: ' + err);
                    }
                    else {
                        socket.emit('eventsListLoad', data);
                    }
                });
            }
        }

        function loadAllNews() {
            db.news.find({}, function(err, data) {
                if (err || !data.length) {
                    console.log('[' + new Date() + '] Cound not find all news Error: ' + err);
                }
                else {
                    socket.emit('allNewsLoad', data);
                }
            });
        }

        function setupSIOFU() {
            if (localUser.Access === 5) {
                // Make an instance of SocketIOFileUpload and listen on this socket:
                var uploader = new SocketIOFileUpload();
                uploader.dir = "./public/images/uploads";
                uploader.listen(socket);

                // Do something when a file is saved:
                uploader.on("saved", function(event) {
                    console.log(event.file);
                    var pathName = event.file.pathName;
                    if (pathName.substring(pathName.length - 3) === 'mp4') {
                        socket.emit('closeNewsModal');
                        loadAllNews();
                    }

                });

                // Error handler:
                uploader.on("error", function(event) {
                    console.log("Error from uploader", event);
                });
            }
        }

        function loadHomepageNumbers() {
            db.projects.count({
                stage: 'Scout',
            }, function(error, scoutNumber) {
                db.projects.count({
                    stage: 'Alpha',
                    status: 'Active'
                }, function(error, alphaNumber) {
                    db.projects.count({
                        stage: 'Beta',
                        status: 'Active'
                    }, function(error, betaNumber) {
                        db.projects.count({
                            stage: 'Live',
                            status: 'Active'
                        }, function(error, liveNumber) {
                            socket.emit('homepageNumberResult', scoutNumber, alphaNumber, betaNumber, liveNumber)
                        });
                    });
                });
            });
        }

        /*
         ** Projects Commands
         */

        socket.on('saveProject', function(newDataArray, projectRef) {
            if (localUser.Access >= 4) {
                console.log('[' + new Date() + '] Saving Data by ' + localUser.username);
                // $$hashKey is a key that angular adds but mongo does not like it
                newDataArray.forEach(function(project) {
                    delete project.$$hashKey;
                    project.Component.forEach(function(component) {
                        delete component.$$hashKey
                    });
                });

                db.projects.remove({
                    reference: projectRef
                }, function(err) {
                    if (err) {
                        console.log('error deleting projects Error: ' + err)
                    }
                    else {
                        db.projects.insert(newDataArray, function(err) {
                            if (err) {
                                console.log('error inserting projects Error: ' + err);
                            }
                            else {
                                socket.broadcast.emit('updateData', newDataArray, projectRef);
                            }
                        });
                    }
                });
            }
            else {
                console.log('[' + new Date() + '] Wrong access level. ' + localUser.username + ' attempted to save project data');
            }
        }); // end of saveProject socket

        /*
         ** Admin Server Commands
         */

        socket.on('findAllUserPermissionsRequest', function() {
            if (localUser.Access === 5) {
                db.users.find({}, function(err, data) {
                    if (err || !data.length) {
                        console.log('[' + new Date() + '] Error finding user Permissions. Error: ' + err);
                    }
                    else {
                        socket.emit('findAllUserPermissionsResult', data);
                    }
                });
            }
            else {
                console.log('[' + new Date() + '] Wrong access level. ' + localUser.username + ' attempted to download user permissions ');
            }
        }); // end of findAllUserPermissionsRequest socket

        socket.on('changeAccessLevel', function(user, accessLevel) {
            if (localUser.Access === 5) {
                console.log('[' + new Date() + '] Changing Access Level for user ' + user.username + ' to level: ' + accessLevel + ' by user: ' + localUser.username);
                db.users.update({
                    _id: user._id
                }, {
                    $set: {
                        Access: accessLevel
                    }
                });
            }
            else {
                console.log('[' + new Date() + '] Wrong access level. ' + localUser.username + ' attempted to change opco level ');
            }
        }); // end of changeAccessLevel socket

        socket.on('deleteUser', function(user) {
            if (localUser.Access === 5) {
                console.log('[' + new Date() + '] Deleting user: ' + user.username + ' from the database');
                db.users.remove({
                    _id: user._id
                });
            }
            else {
                console.log('[' + new Date() + '] Wrong access level. ' + localUser.username + ' attempted to delete user ' + user.username);
            }
        }); // end of deleteUser socket

        socket.on('addNewUser', function(user) {
            if (localUser.Access === 5) {
                user.password = createHash(user.password);
                user._id = (1e4 * (Date.now() + Math.random())).toString(16);
                db.users.findOne({
                    username: user.username
                }, function(err, data) {
                    if (err) {
                        // error finding user
                        console.log('[' + new Date() + '] Error adding new user (admin page) findOne error: ' + err);
                        socket.emit('addNewUserResponse', false, 'There has been a problem, please try again later');
                    }
                    if (data) {
                        // user exists already
                        console.log('[' + new Date() + '] Error adding new user (admin page) already exists');
                        socket.emit('addNewUserResponse', false, 'Username already exists');
                    }
                    else {
                        db.users.insert(user, function(err) {
                            if (err) {
                                console.log('Error adding new user (admin page) insert error: ' + err);
                                socket.emit('addNewUserResponse', false, 'There has been a problem, please try again later');
                            }
                            else {
                                // Success
                                socket.emit('addNewUserResponse', true, 'User successfully added');
                            }
                        }); // end of insert
                    }
                }); // end of findOne
            }
            else {
                console.log('[' + new Date() + '] Wrong access level. ' + localUser.username + ' attempted add a new user');
            }
        }); // end of addNewUser socket


        socket.on('emailDomainListRequest', function() {
            if (localUser.Access === 5) {
                db.emailDomain.find({}, function(err, data) {
                    if (err) console.log("error finding email domains, Error: " + err);

                    socket.emit('emailDomainListResult', data);
                });
            }
            else {
                console.log('[' + new Date() + '] Wrong access level. ' + localUser.username + ' attempted download emailDomainList');
            }
        });
        socket.on('addEmailDomain', function(domain) {
            if (localUser.Access === 5) {
                db.emailDomain.insert(domain);
            }
            else {
                console.log('[' + new Date() + '] Wrong access level. ' + localUser.username + ' attempted to add an email domain');
            }
        }); // end of addEmailDomain

        socket.on('deleteEmailDomain', function(id) {
            if (localUser.Access === 5) {
                db.emailDomain.remove({
                    _id: id
                });
            }
            else {
                console.log('[' + new Date() + '] Wrong access level. ' + localUser.username + ' attempted to delete an email domain');
            }
        }); // end of deleteEmailDomain


        socket.on('backupDataRequest', function(table) {
            if (localUser.Access === 5) {
                if (table === 'projectsBackup') {
                    db.projects.find({}, function(err, projectsData) {
                        if (err || !projectsData.length) {
                            console.log('[' + new Date() + '] Error backing up the projects data Error: ' + err);
                        }
                        else {
                            socket.emit('backupDataResult', projectsData, table);
                        }
                    });
                }
                else if (table === 'eventsBackup') {
                    db.events.find({}, function(err, eventsData) {
                        if (err || !eventsData.length) {
                            console.log('[' + new Date() + '] Error backing up the events data Error: ' + err);
                        }
                        else {
                            socket.emit('backupDataResult', eventsData, table);
                        }
                    });

                }
                else if (table === 'newsBackup') {
                    db.news.find({}, function(err, newsData) {
                        if (err || !newsData.length) {
                            console.log('[' + new Date() + '] Error backing up the news data Error: ' + err);
                        }
                        else {
                            socket.emit('backupDataResult', newsData, table);
                        }
                    });

                }

            }
            else {
                console.log('[' + new Date() + '] Wrong access level. ' + localUser.username + ' attempted to backup data ');
            }

        }); // end of backupDataRequest socket

        socket.on('restoreData', function(table, data) {
            if (localUser.Access === 5) {
                if (table === 'projectsRestore') {
                    db.projects.remove({}, function(err) {
                        if (err) {
                            console.log('[' + new Date() + '] Error Restoring Projects Delete Error: ' + err);
                        }
                        else {
                            db.projects.insert(data, function(err) {
                                if (err) {
                                    console.log('[' + new Date() + '] Error Restoring Projects Insert Error: ' + err);
                                }
                                else {
                                    socket.emit('finishedRestore', table);
                                }
                            });
                        }
                    });
                }
                else if (table === 'eventsRestore') {
                    db.events.remove({}, function(err) {
                        if (err) {
                            console.log('[' + new Date() + '] Error Restoring Events Delete Error: ' + err);
                        }
                        else {
                            db.events.insert(data, function(err) {
                                if (err) {
                                    console.log('[' + new Date() + '] Error Restoring Events Insert Error: ' + err);
                                }
                                else {
                                    socket.emit('finishedRestore', table);
                                }
                            });
                        }
                    });
                }
                else if (table === 'newsRestore') {
                    db.news.remove({}, function(err) {
                        if (err) {
                            console.log('[' + new Date() + '] Error Restoring News Delete Error: ' + err);
                        }
                        else {
                            db.news.insert(data, function(err) {
                                if (err) {
                                    console.log('[' + new Date() + '] Error Restoring News Insert Error: ' + err);
                                }
                                else {
                                    socket.emit('finishedRestore', table);
                                }
                            });
                        }
                    });
                }
            }
            else {
                console.log('[' + new Date() + '] Wrong access level. ' + localUser.username + ' attempted to restore data ');
            }
        }); // end of restoreData Socket


        /*
         ** News Page
         */

        socket.on('saveArticle', function(data) {
            if (localUser.Access === 5) {
                db.news.insert(data, function(err) {
                    if (err) {
                        console.log('[' + new Date() + '] error inserting article Error: ' + err);
                    }
                });
            }
            else {
                console.log('[' + new Date() + '] Wrong access level. ' + localUser.username + ' attempted to add a new article');
            }

        });

        socket.on('deleteArticle', function(id) {
            if (localUser.Access === 5) {
                db.news.remove({
                    _id: id
                }, function(err) {
                    if (err) {
                        console.log('[' + new Date() + '] error deleting news article Error: ' + err);
                    }
                    else {
                        loadAllNews();
                    }
                });
            }
            else {
                console.log('[' + new Date() + '] Wrong access level. ' + localUser.username + ' attempted to delete an article');
            }
        }); // end of deleteArticle socket

        socket.on('editArticle', function(article) {
            if (localUser.Access === 5) {
                db.news.update({
                    _id: article._id
                }, {
                    $set: {
                        title: article.title,
                        body: article.body,
                        date: article.date,
                        selectDate: article.selectDate
                    }
                }, function(err) {
                    if (err) {
                        console.log('[' + new Date() + '] error updating new article Error: ' + err);
                    }
                    else {
                        loadAllNews();
                    }
                });
            }
            else {
                console.log('[' + new Date() + '] Wrong access level. ' + localUser.username + ' attempted to edit an article');
            }
        }); // end of editArticle socket




        /*
         ** Events Functions
         */

        socket.on('addEvent', function(newEvent) {
            if (localUser.Access === 5) {
                console.log('inserting event')
                db.events.insert(newEvent);
            }
            else {
                console.log('[' + new Date() + '] Wrong access level. ' + localUser.username + ' attempted to add an event');
            }
        }); // end of addEvent Socket

        socket.on('deleteEvent', function(eventID) {
            if (localUser.Access === 5) {
                console.log('deleting event')
                db.events.remove({
                    _id: eventID
                });
            }
            else {
                console.log('[' + new Date() + '] Wrong access level. ' + localUser.username + ' attempted to delete an event');
            }
        }); // end of deleteEvent Socket

        socket.on('editEvent', function(editedEvent) {
            if (localUser.Access === 5) {
                console.log('updating event');
                db.events.update({
                    _id: editedEvent._id
                }, {
                    $set: {
                        title: editedEvent.title,
                        description: editedEvent.description,
                        start: editedEvent.start,
                        end: editedEvent.end,
                        className: editedEvent.className,
                        displayEndDate: editedEvent.displayEndDate
                    }
                }, function(err) {
                    if (err) {
                        console.log('error updating event error: ' + err);
                    }
                    else {
                        loadEvents(localUser.Access);
                    }
                });
            }
            else {
                console.log('[' + new Date() + '] Wrong access level. ' + localUser.username + ' attempted to edit an event');
            }
        }); // end of editEvent Socket

        /*
         ** Profile Page
         */

        socket.on('profileDetailsRequest', function() {

            var user = {
                firstName: localUser.firstName,
                lastName: localUser.lastName,
                username: localUser.username,
                OpCo: localUser.OpCo

            }
            socket.emit('profileDetailsResult', user)
        }); // end of profileDetailsRequest Socket

        socket.on('changePassword', function(oldPass, newPass) {
            console.log(localUser.loginMethod)
            if (localUser.loginMethod !== 'bSafe') {
                db.users.findOne({
                    _id: localUser._id
                }, {
                    password: 1
                }, function(err, data) {
                    if (err) {
                        console.log('error finding password to change error: ' + err);
                    }
                    else if (data === null) {
                        socket.emit('changePasswordResponse', 'Something went wrong')
                    }
                    else {


                        if (!isValidPassword(data, oldPass)) {
                            console.log('Passwords do not match!')
                            socket.emit('changePasswordResponse', 'Old password is incorrect. Please re-enter the correct password', false)
                        }
                        else {
                            console.log('they match!');
                            db.users.update({
                                username: localUser.username
                            }, {
                                $set: {
                                    password: createHash(newPass)
                                }
                            }, function(err) {
                                if (err)
                                    console.log('error updating new password error: ' + err);

                                socket.emit('changePasswordResponse', 'Password Updated', true);

                            });
                        }
                    }
                });
            }
            else {
                socket.emit('changePasswordResponse', 'Error', false)
            }


        }); // end of changePasswork Socket


        /*
         ** Site Stats
         */

        socket.on('siteStatsInsert', function(page, mobile) {

            if (localUser.username === undefined) {
                console.log('login error?')
                socket.emit('loginError');
            }
            else if (localUser.OpCo === undefined) {}
            else {
                var date = new Date().toISOString();
                console.log(date + ' name: ' + localUser.firstName + ' page: ' + page);
                var newHit = {
                    _id: (1e4 * (Date.now() + Math.random())).toString(16),
                    username: localUser.username,
                    opCo: localUser.OpCo,
                    page: page,
                    date: date,
                    mobile: mobile
                }

                db.siteStats.insert(newHit)
            }

        }); // end of siteStatsInsert

        /*
         ** Analytics Page
         */

        socket.on('siteStatsPageViewsRequest', function(dateFrom, dateTo) {
            if (localUser.Access >= 3) {
                console.log(dateFrom + ' ' + dateTo)
                db.siteStats.find({
                    date: {
                        $gte: dateFrom,
                        $lte: dateTo
                    }
                }, function(err, data) {
                    if (err) {
                        console.log('Error finding site stats, Error: ' + err);
                    }
                    else {

                        data = _.sortBy(data, 'page');
                        var hits = {};
                        var hitPages = _.pluck(data, 'page');
                        var pages = _.uniq(hitPages);
                        var hitOpCos = _.pluck(data, 'opCo');
                        hitOpCos = _.uniq(hitOpCos);

                        hitOpCos.forEach(function(opCo) {
                            hits[opCo] = {};
                            pages.forEach(function(page) {
                                if (hits[opCo][page] === undefined) {
                                    hits[opCo][page] = 0;
                                }
                            });
                        });

                        hitOpCos.forEach(function(opCo) {
                            data.forEach(function(hit) {
                                if (hit.opCo === opCo) {
                                    hits[opCo][hit.page]++;
                                }
                            });
                        });


                        socket.emit('siteStatsPageViewsResult', hits, hitOpCos, pages);
                    }
                });
            }
            else {
                console.log('[' + new Date() + '] Wrong access level. ' + localUser.username + ' attempted to view site stats data');
            }

        }); // end of siteStatsPageViewsRequest Socket

        socket.on('siteStatsPopularUserRequest', function() {
            if (localUser.Access >= 3) {
                db.siteStats.distinct("username", function(err, distinctUsers) {
                    if (err) console.log('error finding distinct popular user error: ' + err);

                    distinctUsers.forEach(function(user) {
                        db.siteStats.count({
                            username: user
                        }, function(err, count) {
                            if (err) console.log('error doing a count on distinct popular user error: ' + err);
                            db.users.findOne({
                                username: user
                            }, function(err, userDetails) {
                                if (err) console.log('error finding user details in popularUserList ', err)
                                if (userDetails === null) {
                                    var name = 'n/a'
                                }
                                else {
                                    var name = userDetails.firstName + ' ' + userDetails.lastName

                                }
                                var newObj = {
                                    username: user,
                                    count: count,
                                    fullName: name
                                };
                                socket.emit('siteStatsPopularUserResult', newObj);

                            })

                        });

                    });

                });
            }
            else {
                console.log('[' + new Date() + '] Wrong access level. ' + localUser.username + ' attempted to view popular user data');
            }

        }); // end of siteStatsPopularUser socket

        socket.on('siteStatsTrafficSourcesRequest', function() {
            if (localUser.Access >= 3) {
                db.siteStats.count({
                    mobile: false
                }, function(err, desktopCount) {
                    if (err) console.log('error counting desktop traffic sources ' + err);

                    db.siteStats.count({
                        mobile: true
                    }, function(err, mobileCount) {
                        if (err) console.log('error counting mobile traffic sources');
                        socket.emit('siteStatsTrafficSourcesResult', desktopCount, mobileCount);
                    });
                });
            }
            else {
                console.log('[' + new Date() + '] Wrong access level. ' + localUser.username + ' attempted to view traffic sources data');
            }
        }); // end of siteStatsTrafficSouresRequest socket

        socket.on('siteStatsActivtyPerOpcoRequest', function() {
            if (localUser.Access >= 3) {
                var opcoActivty = {};
                db.siteStats.distinct("opCo", function(err, OpCos) {
                    if (err) console.log('error finding activty per opco distinct error: ' + err);

                    OpCos.forEach(function(opco) {

                        db.siteStats.count({
                            opCo: opco
                        }, function(err, count) {

                            if (err) console.log('error finding activty per opco count error: ' + err);
                            socket.emit('siteStatsActivtyPerOpcoResult', [opco, count]);

                        }); // end of count
                    }); // end of forEach
                }); // end of distinct
            }
            else {
                console.log('[' + new Date() + '] Wrong access level. ' + localUser.username + ' attempted to view site activty per opco data');
            }
        }); // end of siteStatsActivtyPerOpcoRequest socket

        socket.on('howManyOnlineCountRequest', function() {
            if (localUser.Access >= 3) {

                var connectedSockets = io.sockets.sockets;
                var users = [];
                connectedSockets.forEach(function(connectedSocket) {
                    users.push({
                        username: connectedSocket.username,
                        name: connectedSocket.fullName
                    });
                });

                var numberOfClients = io.engine.clientsCount;
                socket.emit('howManyOnlineCountResult', numberOfClients, users);
            }
            else {
                console.log('[' + new Date() + '] Wrong access level. ' + localUser.username + ' attempted to view how many people are online');
            }
        });

        socket.on('findAnalyticsOfProjects', function(filterStatusArray) {
            if (localUser.Access >= 3) {
                db.projects.aggregate({
                    $match: {
                        stage: {
                            $in: ['Scout', 'Alpha', 'Beta', 'Live']
                        },
                        status: {
                            $in: filterStatusArray
                        }
                    }
                }, {
                    $group: {
                        _id: "$reference",
                        dates: {
                            $push: {
                                startDate: "$projectStartDate",
                                endDate: "$projectEndDate",
                                stage: "$stage",
                                title: "$title"
                            }
                        }
                    },
                }, function(err, result) {
                    if (err) console.log('Error finding analyticsOfProjects ', err);
                    else {
                        socket.emit('foundAnalyticsOfProjects', result);
                    }
                })
            }
            else {
                console.log('[' + new Date() + '] Wrong access level. ' + localUser.username + ' attempted to view project analytics');
            }
        });


    }); // end of socket on
};

// Generates hash using bCrypt
var createHash = function(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};
var isValidPassword = function(user, password) {
    return bCrypt.compareSync(password, user.password);
};